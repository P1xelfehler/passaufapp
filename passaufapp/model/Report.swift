//
//  Report.swift
//  passaufapp
//
//  Created by Norman Laudien on 23.11.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import CoreLocation

struct Report: Hashable {
    var id: String
    let userId: String?
    let type: ReportType?
    var message: String?
    let thoroughfare: String?
    let subThoroughfare: String?
    let locality: String?
    let postalCode: Int?
    let location: CLLocation
    var validations: [String]
    var title: String? {
        if let locality = locality {
            if let thoroughfare = thoroughfare {
                if let subThoroughfare = subThoroughfare {
                    return "\(locality), \(thoroughfare) \(subThoroughfare)"
                } else {
                    return "\(locality), \(thoroughfare)"
                }
            } else {
                return "\(locality)"
            }
        }
        return nil
    }
    var hashValue: Int {
        return id.hashValue
    }
    var dictionary: [String: Any] {
        var dictionary = [String: Any]()
        dictionary[ReportPath.userId] = userId
        dictionary[ReportPath.type] = type?.rawValue
        dictionary[ReportPath.message] = message
        dictionary[ReportPath.thoroughfare] = thoroughfare
        dictionary[ReportPath.subThoroughfare] = subThoroughfare
        dictionary[ReportPath.locality] = locality
        dictionary[ReportPath.postalCode] = postalCode
        return dictionary
    }
    var applicationContextDictionary: [String: Any] {
        var dictionary = self.dictionary
        dictionary[ReportPath.latitude] = location.coordinate.latitude
        dictionary[ReportPath.longitude] = location.coordinate.longitude
        return dictionary
    }
    
    init(id: String, userId: String?, type: ReportType?, message: String?, thoroughfare: String?, subThoroughfare: String?, locality: String?, postalCode: Int?, location: CLLocation) {
        self.id = id
        self.userId = userId
        self.type = type
        self.message = message
        self.thoroughfare = thoroughfare
        self.subThoroughfare = subThoroughfare
        self.locality = locality
        self.postalCode = postalCode
        self.location = location
        self.validations = []
    }
    
    init(fromKey id: String, andValue dictionary: [String: AnyObject], withLocation location: CLLocation) {
        self.id = id
        userId = dictionary[ReportPath.userId] as? String
        if let typeInt = dictionary[ReportPath.type] as? Int {
            type = ReportType(rawValue: typeInt)
        } else {
            type = nil
        }
        message = dictionary[ReportPath.message] as? String
        thoroughfare = dictionary[ReportPath.thoroughfare] as? String
        subThoroughfare = dictionary[ReportPath.subThoroughfare] as? String
        locality = dictionary[ReportPath.locality] as? String
        postalCode = dictionary[ReportPath.postalCode] as? Int
        self.location = location
        self.validations = dictionary[ReportPath.validations] as? [String] ?? []
    }
    
    static func == (lhs: Report, rhs: Report) -> Bool {
        return lhs.id == rhs.id
    }
}

struct ReportPath {
    static let reportId = "reportId" // only used in notification payload
    static let userId = "userId"
    static let type = "type"
    static let message = "message"
    static let thoroughfare = "thoroughfare"
    static let subThoroughfare = "subThoroughfare"
    static let locality = "locality"
    static let postalCode = "postalCode"
    static let latitude = "latitude"
    static let longitude = "longitude"
    static let validations = "validations"
}
