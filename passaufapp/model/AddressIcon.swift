//
//  AddressIcon.swift
//  passaufapp
//
//  Created by Norman Laudien on 30.11.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import UIKit

enum AddressIcon: Int {
    case house = 0
    case factory = 1
    case island = 2
    case questionMark = 3
    case car = 4
    case wine = 5
    case boat = 6
    case swing = 7
    
    var image: UIImage {
        switch self {
        case .boat:
            return UIImage(named: "boat")!
        case .car:
            return UIImage(named: "car")!
        case .factory:
            return UIImage(named: "factory")!
        case .house:
            return UIImage(named: "house")!
        case .island:
            return UIImage(named: "island")!
        case .questionMark:
            return UIImage(named: "questionMark")!
        case .swing:
            return UIImage(named: "swing")!
        case .wine:
            return UIImage(named: "wine")!
        }
    }
}
