//
//  ReportType.swift
//  passaufapp
//
//  Created by Norman Laudien on 26.11.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import Foundation

enum ReportType: Int, Codable {
    case group = 0
    case vehicle = 1
    case child = 2
    case theft = 3
    case animal = 4
    case burglary = 5
    case disputes = 6
    case pollution = 7
    case fire = 8
    case lostItems = 9
    case other = 10
    
    var title: String {
        switch self {
        case .group:
            return NSLocalizedString("Auffällige Person / Gruppe", comment: "")
        case .vehicle:
            return NSLocalizedString("Auffälliges Fahrzeug", comment: "")
        case .child:
            return NSLocalizedString("Kind", comment: "")
        case .theft:
            return NSLocalizedString("Diebstahl", comment: "")
        case .animal:
            return NSLocalizedString("Tier in Gefahr", comment: "")
        case .burglary:
            return NSLocalizedString("Einbruch", comment: "")
        case .disputes:
            return NSLocalizedString("Streitigkeiten", comment: "")
        case .pollution:
            return NSLocalizedString("Umweltverschmutzung", comment: "")
        case .fire:
            return NSLocalizedString("Feuer", comment: "")
        case .lostItems:
            return NSLocalizedString("Verlorenes", comment: "")
        case .other:
            return NSLocalizedString("Sonstiges", comment: "")
        }
    }
}
