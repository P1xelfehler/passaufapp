//
//  Address.swift
//  passaufapp
//
//  Created by Norman Laudien on 29.11.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import CoreLocation

struct Address: Hashable {
    let id: String
    let radius: Int? // in meters
    let icon: AddressIcon?
    let location: CLLocation
    let shorty: String?
    var dictionary: [String: Any] {
        var dictionary = [String: Any]()
        dictionary[AddressPath.radius] = radius
        dictionary[AddressPath.icon] = icon?.rawValue
        dictionary[AddressPath.shorty] = shorty
        return dictionary
    }
    
    init(id: String, radius: Int?, icon: AddressIcon, location: CLLocation) {
        self.id = id
        self.radius = radius
        self.icon = icon
        self.location = location
        self.shorty = nil
    }
    
    init(id: String, radius: Int?, shorty: String, location: CLLocation) {
        self.id = id
        self.radius = radius
        self.icon = nil
        self.location = location
        self.shorty = shorty
    }
    
    init(fromKey id: String, andValue dictionary: [String: AnyObject], withLocation location: CLLocation) {
        self.id = id
        self.radius = dictionary[AddressPath.radius] as? Int
        if let iconInt = dictionary[AddressPath.icon] as? Int {
            self.icon = AddressIcon(rawValue: iconInt)
        } else {
            icon = nil
        }
        self.location = location
        self.shorty = dictionary[AddressPath.shorty] as? String
    }
}

struct AddressPath {
    static let radius = "radius"
    static let icon = "icon"
    static let shorty = "shorty"
}
