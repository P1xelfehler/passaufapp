//
//  StoreUtils.swift
//  passaufapp
//
//  Created by Norman Laudien on 18.12.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import Firebase
import StoreKit

class StoreUtils: NSObject {
    static let shared = StoreUtils()
    private let PRODUCT_ID = "ProVersion12Monate"
    private var mProducts = [SKProduct]()
    var alertHandler: ((StoreUtilsAlertType) -> ())?
    var errorHandler: ((Error) -> ())?
    
    static func isPremium(_ completion: @escaping (Bool) -> ()) {
        guard let userId = Auth.auth().currentUser?.uid else {
            completion(false)
            return
        }
        let ref = Database.database()
            .reference(withPath: "users")
            .child(userId)
            .child("premium")
        ref.keepSynced(true)
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            let isPremium = (snapshot.value as? Bool) == true
            completion(isPremium)
        }) { (_) in
            completion(false)
        }
    }
    
    private func buyPremium() {
        guard Auth.auth().currentUser?.isEmailVerified == true else {
            alertHandler?(.emailNotVerified)
            return
        }
        guard SKPaymentQueue.canMakePayments() else {
            alertHandler?(.purchasesDisabled)
            return
        }
        guard let product = mProducts.first else {
            alertHandler?(.purchaseFailed)
            return
        }
        let payment = SKPayment(product: product)
        let queue = SKPaymentQueue.default()
        queue.add(self)
        queue.add(payment)
    }
    
    private func enablePremium() {
        // TODO: verify receipt on server before activating premium
        guard let userId = Auth.auth().currentUser?.uid else { return }
        Database.database()
            .reference(withPath: "users")
            .child(userId)
            .child("premium")
            .setValue(true)
    }
    
    func fetchAvailableProducts() {
        let productsRequest = SKProductsRequest(productIdentifiers: [PRODUCT_ID])
        productsRequest.delegate = self
        productsRequest.start()
    }
    
    func restorePurchases() {
        guard Auth.auth().currentUser?.isEmailVerified == true else {
            alertHandler?(.emailNotVerified)
            return
        }
        let queue = SKPaymentQueue.default()
        queue.add(self)
        queue.restoreCompletedTransactions()
    }
    
    func showBuyAlertController(in viewController: UIViewController) {
        let alertController = UIAlertController(
            title: NSLocalizedString("Pro Version", comment: ""),
            message: NSLocalizedString("Die Detail Ansicht ist in der Pro Version verfügbar. Sie können diese als 12 Monats Abo kaufen. Das Abo verlängert sich automatisch nach 12 Monaten. Sie können es jederzeit im AppStore kündigen.", comment: ""),
            preferredStyle: .alert
        )
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Abbrechen", comment: ""), style: .cancel))
        let buyAction = UIAlertAction(title: NSLocalizedString("Kaufen", comment: ""), style: .default, handler: { [weak viewController] (_) in
            viewController?.showLoadingView()
            self.buyPremium()
        })
        alertController.addAction(buyAction)
        alertController.preferredAction = buyAction
        viewController.present(alertController, animated: true)
    }
}

extension StoreUtils: SKProductsRequestDelegate {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        mProducts = response.products
        print("number of products: \(response.products.count)")
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        errorHandler?(error)
    }
}

extension StoreUtils: SKPaymentTransactionObserver {
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .failed:
                if let error = transaction.error {
                    errorHandler?(error)
                } else {
                    alertHandler?(.purchaseFailed)
                }
                queue.finishTransaction(transaction)
            case .purchased:
                enablePremium()
                alertHandler?(.purchaseSuccessful)
                queue.finishTransaction(transaction)
            case .restored:
                enablePremium()
                alertHandler?(.purchasesRestored)
                queue.finishTransaction(transaction)
            case .deferred, .purchasing:
                break
            }
        }
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        enablePremium()
        alertHandler?(.purchasesRestored)
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        errorHandler?(error)
    }
}

enum StoreUtilsAlertType {
    case emailNotVerified
    case purchaseFailed
    case purchasesRestored
    case purchasesDisabled
    case purchaseSuccessful
    
    var title: String {
        switch self {
        case .emailNotVerified:
            return NSLocalizedString("Email not verified", comment: "")
        case .purchaseFailed:
            return NSLocalizedString("Purchase failed", comment: "")
        case .purchasesRestored:
            return NSLocalizedString("Purchases restored", comment: "")
        case .purchasesDisabled:
            return NSLocalizedString("Purchases disabled", comment: "")
        case .purchaseSuccessful:
            return NSLocalizedString("Purchase Successful", comment: "")
        }
    }
    
    var message: String {
        switch self {
        case .emailNotVerified:
            return NSLocalizedString("Your Email is not verified. You cannot make any purchases.", comment: "")
        case .purchaseFailed:
            return NSLocalizedString("The purchase failed. Please try again.", comment: "")
        case .purchasesRestored:
            return NSLocalizedString("You have successfully restored your purchases.", comment: "")
        case .purchasesDisabled:
            return NSLocalizedString("Purchases are disabled on your device.", comment: "")
        case .purchaseSuccessful:
            return NSLocalizedString("You have successfully bought premium.", comment: "")
        }
    }
}
