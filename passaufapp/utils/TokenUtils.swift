//
//  TokenUtils.swift
//  leximaster
//
//  Created by Norman Laudien on 24.02.18.
//  Copyright © 2018 Norman Laudien. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase
import FirebaseMessaging

class TokenUtils {
    private static let instanceTokenKey = "instanceToken"
    
    static var currentToken: String? {
        return Messaging.messaging().fcmToken
    }
    
    class func uploadToken(of user: User) {
        guard let token = Messaging.messaging().fcmToken else { return }
        upload(token: token, of: user)
    }
    
    class func removeToken(of user: User) {
        if let token = currentToken {
            remove(token: token, of: user)
        }
    }
    
    class func update(token: String, of user: User) {
        if let oldToken = UserDefaults.standard.string(forKey: instanceTokenKey) {
            remove(token: oldToken, of: user)
        }
        upload(token: token, of: user)
    }
    
    class func remove(token: String, of user: User) {
        Database.database()
            .reference(withPath: "users")
            .child(user.uid)
            .child("tokens")
            .child(token)
            .removeValue()
    }
    
    private class func upload(token: String, of user: User) {
        UserDefaults.standard.setValue(token, forKey: instanceTokenKey)
        let value: [String: Any?] = [
            "device": UIDevice.current.name,
            "timestamp": ServerValue.timestamp()
        ]
        Database.database()
            .reference(withPath: "users")
            .child(user.uid)
            .child("tokens")
            .child(token)
            .setValue(value)
    }
}
