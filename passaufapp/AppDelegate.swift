//
//  AppDelegate.swift
//  passaufapp
//
//  Created by Norman Laudien on 23.11.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import UIKit
import WatchConnectivity
import UserNotifications
import Firebase
import FirebaseUI
import GoogleSignIn
import GeoFire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    private var mAuthHandle: AuthStateDidChangeListenerHandle?
    var window: UIWindow?
    var clickedReport: Report? // the report of the notification that was clicked
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window?.tintColor = UIColor(named: "Tint")
        FirebaseApp.configure()
        configureFirebaseAuthUI()
        Auth.auth().useAppLanguage()
        Database.database().isPersistenceEnabled = true
        configureGoogleSignIn()
        configureMessaging(for: application)
        StoreUtils.shared.fetchAvailableProducts()
        registerAuthStateDidChangeListener()
        setupWatchConnectivity()
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        updateApplicationContext()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        unregisterAuthStateDidChangeListener()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        updateApplicationContext {
            completionHandler(.newData)
        }
    }
    
    // MARK: - Other functions
    
    func registerAuthStateDidChangeListener() {
        mAuthHandle = Auth.auth().addStateDidChangeListener({ [weak self] (_, user) in
            if let user = user {
                self?.updateApplicationContext()
                TokenUtils.uploadToken(of: user)
            } else {
                self?.clearApplicationContext()
            }
        })
    }
    
    func unregisterAuthStateDidChangeListener() {
        if let authHandle = mAuthHandle {
            Auth.auth().removeStateDidChangeListener(authHandle)
        }
    }
}

// MARK: - Watch connectivity

extension AppDelegate: WCSessionDelegate {
    
    private func setupWatchConnectivity() {
        guard WCSession.isSupported() else { return }
        let session = WCSession.default
        session.delegate = self
        session.activate()
    }
    
    func updateApplicationContext(_ completionHandler: (() -> ())? = nil) {
        guard let userId = Auth.auth().currentUser?.uid else { return }
        let session = WCSession.default
        guard session.isPaired && session.isWatchAppInstalled else { return }
        Database.database()
            .reference(withPath: "users")
            .child(userId)
            .child("premium")
            .observeSingleEvent(of: .value) { [weak self] (snapshot) in
                let isPremium = (snapshot.value as? Bool) == true
                if isPremium { // user is premium -> fetch reports of all addresses
                    self?.clearApplicationContext()
                    let ref = snapshot.ref.parent?.child("addresses")
                    ref?.keepSynced(true)
                    ref?.observeSingleEvent(of: .value, with: { (snapshot) in
                        for child in snapshot.children {
                            guard let child = child as? DataSnapshot else { continue }
                            guard let radius = child.childSnapshot(forPath: "radius").value as? Double else { continue }
                            guard let coordinates = child.childSnapshot(forPath: "l").value as? [CLLocationDegrees] else { continue }
                            let addressLocation = CLLocation(latitude: coordinates[0], longitude: coordinates[1])
                            let reportsRef = Database.database().reference(withPath: "reports")
                            reportsRef.keepSynced(true)
                            let geoFire = GeoFire(firebaseRef: reportsRef)
                            let query = geoFire.query(at: addressLocation, withRadius: radius / 1000)
                            query.observeReady({
                                query.removeAllObservers()
                            })
                            query.observe(.keyEntered, with: { (reportId, reportLocation) in
                                reportsRef.child(reportId).observeSingleEvent(of: .value, with: { (snapshot) in
                                    guard let value = snapshot.value as? [String: AnyObject] else { return }
                                    let report = Report(
                                        fromKey: reportId,
                                        andValue: value,
                                        withLocation: reportLocation
                                    )
                                    self?.appendReport(report, toApplicationContextInSession: session)
                                })
                            })
                        }
                        completionHandler?()
                    })
                } else { // user not premium -> fetch only own reports
                    let ref = Database.database()
                        .reference(withPath: "reports")
                        .queryOrdered(byChild: "userId")
                        .queryEqual(toValue: userId)
                    ref.keepSynced(true)
                    ref.observeSingleEvent(of: .value, with: { (snapshot) in
                        var reports = [Report]()
                        for child in snapshot.children {
                            guard let child = child as? DataSnapshot else { continue }
                            guard let value = child.value as? [String: AnyObject] else { continue }
                            guard let coordinates = child.childSnapshot(forPath: "l").value as? [CLLocationDegrees] else { continue }
                            let location = CLLocation(latitude: coordinates[0], longitude: coordinates[1])
                            let report = Report(
                                fromKey: child.key,
                                andValue: value,
                                withLocation: location
                            )
                            reports.append(report)
                        }
                        self?.updateApplicationContext(inSession: session, withReports: reports)
                        completionHandler?()
                    })
                }
        }
    }
    
    func updateApplicationContext(inSession session: WCSession, withReports reports: [Report]) {
        do {
            var reportDicts = [String: [String: Any]]()
            for report in reports {
                reportDicts[report.id] = report.applicationContextDictionary
            }
            try session.updateApplicationContext(["reports": reportDicts])
            print("Updated application context with \(reports.count) reports!")
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func appendReport(_ report: Report, toApplicationContextInSession session: WCSession) {
        if var reports = session.applicationContext["reports"] as? [String: [String: Any]] {
            reports[report.id] = report.applicationContextDictionary
            do {
                try session.updateApplicationContext(["reports": reports])
                print("Updated application context with \(reports.count) reports!")
            } catch {
                print(error.localizedDescription)
            }
        } else { // no reports in application context yet
            updateApplicationContext(inSession: session, withReports: [report])
        }
    }
    
    func clearApplicationContext() {
        do {
            try WCSession.default.updateApplicationContext([:])
            print("Application context cleared!")
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        updateApplicationContext()
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func sessionWatchStateDidChange(_ session: WCSession) {
        updateApplicationContext()
    }
}

// MARK: - Google sign in

extension AppDelegate: GIDSignInDelegate {
    func configureGoogleSignIn() {
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
    }
    
    // Google sign in
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil {
            // TODO: handle that error
        } else if let authentication = user.authentication{
            let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
            Auth.auth().signInAndRetrieveData(with: credential, completion: nil)
        }
    }
    
    // google user disconnected
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
}

// MARK: - Authentification

extension AppDelegate: FUIAuthDelegate {
    func configureFirebaseAuthUI() {
        guard let authUI = FUIAuth.defaultAuthUI() else {
            return
        }
        authUI.delegate = self
        authUI.providers = [
            FUIGoogleAuth()
        ]
    }
    
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        if let user = authDataResult?.user {
            // update message token
            TokenUtils.uploadToken(of: user)
            // email verification
            if !user.isEmailVerified {
                user.sendEmailVerification { (error) in
                    if let error = error {
                        print("error sending verification email: \(error.localizedDescription)")
                    } else {
                        print("verification email sent!")
                    }
                }
            }
        }
    }
    
    func authPickerViewController(forAuthUI authUI: FUIAuth) -> FUIAuthPickerViewController {
        return AuthPickerVC(nibName: "AuthPickerVC", bundle: Bundle.main, authUI: authUI)
    }
    
    func emailEntryViewController(forAuthUI authUI: FUIAuth) -> FUIEmailEntryViewController {
        return EmailEntryViewController(authUI: authUI)
    }
    
    func passwordSignInViewController(forAuthUI authUI: FUIAuth, email: String) -> FUIPasswordSignInViewController {
        return PasswordSignInViewController(authUI: authUI, email: email)
    }
    
    func passwordSignUpViewController(forAuthUI authUI: FUIAuth, email: String) -> FUIPasswordSignUpViewController {
        return PasswordSignUpViewController(authUI: authUI, email: email)
    }
    
    func passwordRecoveryViewController(forAuthUI authUI: FUIAuth, email: String) -> FUIPasswordRecoveryViewController {
        return PasswordRecoveryViewController(authUI: authUI, email: email)
    }
}

// MARK: - Messaging

extension AppDelegate: MessagingDelegate, UNUserNotificationCenterDelegate {
    
    func configureMessaging(for application: UIApplication) {
        Messaging.messaging().delegate = self // Firebase messaging
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.delegate = self
        notificationCenter.requestAuthorization(options: [.alert, .sound, .badge, .carPlay]) { (granted, error) in
            if (granted) {
                DispatchQueue.main.async {
                    application.registerForRemoteNotifications()
                }
            }
        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        if let user = Auth.auth().currentUser {
            TokenUtils.update(token: fcmToken, of: user)
        }
    }
    
    // called when a notification and has been clicked
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let payload = response.notification.request.content.userInfo
        didClickNotification(with: payload)
        completionHandler()
    }
    
    // called when a notification was received while the app is in the FOREGROUND
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        updateApplicationContext()
        completionHandler(.alert) // show the notification like when received in background
    }
    
    // user clicked on a notification
    func didClickNotification(with payload: [AnyHashable: Any]) {
        guard let reportId = payload["reportId"] as? String else { return }
        guard let jsonString = payload["report"] as? String else { return }
        guard let jsonData = jsonString.data(using: .utf8) else { return }
        guard let reportData = (try? JSONSerialization.jsonObject(with: jsonData)) as? [String: AnyObject] else { return }
        guard let location = GeoFire.location(fromValue: reportData) else { return }
        let report = Report(
            fromKey: reportId,
            andValue: reportData,
            withLocation: location
        )
        if let homeVC = window?.rootViewController?.children.first?.children.first as? HomeViewController, homeVC.viewIfLoaded != nil {
            homeVC.openReport(report)
        } else { // still loading
            clickedReport = report
        }
    }
}
