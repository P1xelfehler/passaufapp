//
//  HomeViewController.swift
//  passaufapp
//
//  Created by Norman Laudien on 01.11.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import UIKit
import DeepDiff
import Firebase
import GeoFire

class HomeViewController: UIViewController {
    private var mGeoFire: GeoFire!
    private var mAddressItems = [AddressItem]() {
        didSet {
            if mAddressItems.isEmpty {
                mShaking = false
            }
        }
    }
    private var mShaking = false {
        didSet {
            addButton.isEnabled = !mShaking
            if mShaking {
                removeSelection()
            }
        }
    }
    private var mNumberOfReports: UInt = 0 {
        didSet {
            if mNumberOfReports == 0 {
                reportsLabel.text = NSLocalizedString("No reports", comment: "")
                changeHeaderImage(to: UIImage(named: "greenHighlight")!)
            } else {
                reportsLabel.text = "\(mNumberOfReports) \(mNumberOfReports == 1 ? NSLocalizedString("report", comment: "") : NSLocalizedString("reports", comment: ""))"
                changeHeaderImage(to: UIImage(named: "redHighlight")!)
            }
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    private var premiumFeaturesEnabled = true
    var clickedReport: Report?
    
    // MARK: - Outlets
    
    @IBOutlet private weak var addButton: UIButton!
    @IBOutlet private weak var addressesCollectionView: UICollectionView!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet weak var highlightImageView: UIImageView!
    @IBOutlet weak var reportsLabel: UILabel!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerLongPressGestureRecognizers()
        let flowLayout = addressesCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.minimumLineSpacing = 0
        let ref = Database.database().reference(withPath: "reports")
        mGeoFire = GeoFire(firebaseRef: ref)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        StoreUtils.isPremium { (isPremium) in
            self.premiumFeaturesEnabled = isPremium
            self.fetchAddresses()
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let report = appDelegate.clickedReport {
            appDelegate.clickedReport = nil
            openReport(report)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterObservers()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if !mShaking {
            removeSelection()
        }
        mShaking = false
        addressesCollectionView.reloadData()
    }
    
    // MARK: - Actions
    
    @objc func collectionViewLongPressed(_ sender: UILongPressGestureRecognizer) {
        guard sender.state == .began else { return }
        guard !mAddressItems.isEmpty else { return }
        let feedbackGenerator = UIImpactFeedbackGenerator(style: .medium)
        feedbackGenerator.prepare()
        feedbackGenerator.impactOccurred()
        mShaking = true
        addressesCollectionView.reloadData()
    }
    
    private func deleteTapped(_ cell: AddressCollectionViewCell) {
        guard let indexPath = addressesCollectionView.indexPath(for: cell) else { return }
        guard let userId = Auth.auth().currentUser?.uid else { return }
        let item = mAddressItems.remove(at: indexPath.row)
        item.query?.removeObservers()
        addressesCollectionView.deleteItems(at: [indexPath])
        tableView.deleteSections([indexPath.row], with: .automatic)
        Database.database()
            .reference(withPath: "users")
            .child(userId)
            .child("addresses")
            .child(item.address.id)
            .removeValue()
    }
    
    private func addressButtonTapped(_ cell: AddressCollectionViewCell) {
        guard !mShaking else { return }
        guard let indexPath = addressesCollectionView.indexPath(for: cell) else { return }
        let item = mAddressItems[indexPath.row]
        let selected = !cell.isSelected
        cell.isSelected = selected
        if selected { // cell selected
            if mAddressItems.nothingSelected {
                unregisterObservers(butFor: item)
            } else {
                registerObservers(forAddress: item.address, atIndexPath: indexPath)
            }
            item.isSelected = true
        } else { // cell deselected
            item.isSelected = false
            if mAddressItems.nothingSelected { // fetch all reports
                for (index, item) in mAddressItems.enumerated() {
                    let indexPath = IndexPath(row: index, section: 0)
                    registerObservers(forAddress: item.address, atIndexPath: indexPath)
                }
            } else { // unregister address
                item.query?.removeObservers()
                let indexPaths = item.reports.indexPaths(usingSection: indexPath.row)
                item.reports.removeAll()
                tableView.deleteRows(at: indexPaths, with: .automatic)
                self.mNumberOfReports -= UInt(indexPaths.count)
            }
        }
    }
    
    // MARK: - Segues
    
    @IBAction func unwindToHome(_ segue: UIStoryboardSegue) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "reportSegue":
            let destination = segue.destination as! ReportViewController
            if let cell = sender as? UITableViewCell {
                if let indexPath = tableView.indexPath(for: cell) {
                    let report = mAddressItems[indexPath.section].reports[indexPath.row]
                    destination.loadFromSegue(report: report)
                }
            } else if let report = clickedReport {
                destination.loadFromSegue(report: report)
            }
        default:
            break
        }
    }
    
    // MARK: - Other methods
    
    private func registerObservers(forAddress address: Address, atIndexPath indexPath: IndexPath) {
        let geoFireQuery = mGeoFire.query(at: address.location, withRadius: Double(address.radius ?? 500) / 1000)
        let keyEnteredQuery = geoFireQuery.observe(.keyEntered) { (reportId, location) in
            guard !self.mAddressItems.contains(where: { (item) -> Bool in
                return item.reports.contains(where: { (report) -> Bool in report.id == reportId })
            }) else { return }
            self.fetchReport(withId: reportId, atLocation: location, intoSection: indexPath.row)
        }
        let keyExitedQuery = geoFireQuery.observe(.keyExited, with: { (reportId, location) in
            guard let indexPath = self.mAddressItems.indexPath(ofReportWithId: reportId) else { return }
            let item = self.mAddressItems[indexPath.section]
            item.reports.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            self.mNumberOfReports -= 1
        })
        mAddressItems[indexPath.row].query = Query(
            query: geoFireQuery,
            keyEnteredHandle: keyEnteredQuery,
            keyExitedHandle: keyExitedQuery
        )
    }
    
    private func unregisterObservers(butFor exception: AddressItem? = nil) {
        for (section, item) in mAddressItems.enumerated() {
            if let query = item.query {
                guard item.address.id != exception?.address.id else { continue }
                query.removeObservers()
                let indexPaths = item.reports.indexPaths(usingSection: section)
                item.reports.removeAll()
                item.query = nil
                tableView.deleteRows(at: indexPaths, with: .automatic)
            }
        }
        mNumberOfReports = UInt(exception?.reports.count ?? 0)
    }
    
    private func fetchReport(withId id: String, atLocation location: CLLocation, intoSection section: Int) {
        Database.database()
            .reference(withPath: "reports")
            .child(id)
            .observeSingleEvent(of: .value) { (snapshot) in
                guard let value = snapshot.value as? [String: AnyObject] else { return }
                let report = Report(fromKey: snapshot.key, andValue: value, withLocation: location)
                guard self.premiumFeaturesEnabled || report.userId == Auth.auth().currentUser?.uid else { return }
                let item = self.mAddressItems[section]
                guard !item.reports.contains(report) else { return }
                let indexPath = IndexPath(row: item.reports.count, section: section)
                item.reports.append(report)
                self.tableView.insertRows(at: [indexPath], with: .automatic)
                self.mNumberOfReports += 1
        }
    }
    
    private func fetchAddresses() {
        guard let userId = Auth.auth().currentUser?.uid else { return }
        let ref = Database.database()
            .reference(withPath: "users")
            .child(userId)
            .child("addresses")
        ref.keepSynced(true)
        ref.observeSingleEvent(of: .value) { (snapshot) in
            let geoFire = GeoFire(firebaseRef: ref)
            if self.mAddressItems.isEmpty {
                self.mAddressItems.reserveCapacity(Int(snapshot.childrenCount))
            }
            for child in snapshot.children {
                guard let child = child as? DataSnapshot else { continue }
                guard let value = child.value as? [String: AnyObject] else { continue }
                if let index = self.mAddressItems.firstIndex(where: { (item) -> Bool in
                    item.address.id == child.key
                }) { // address is already there
                    let item = self.mAddressItems[index]
                    if item.isSelected || self.mAddressItems.nothingSelected {
                        let indexPath = IndexPath(row: index, section: 0)
                        self.registerObservers(forAddress: item.address, atIndexPath: indexPath)
                    }
                } else { // address is not already there
                    geoFire.getLocationForKey(child.key, withCallback: { (location, _) in
                        guard let location = location else { return }
                        let address = Address(fromKey: child.key, andValue: value, withLocation: location)
                        let indexPath = IndexPath(row: self.mAddressItems.count, section: 0)
                        let item = AddressItem(address: address)
                        self.mAddressItems.append(item)
                        self.addressesCollectionView.insertItems(at: [indexPath])
                        self.tableView.insertSections([indexPath.row], with: .automatic)
                        if self.mAddressItems.nothingSelected {
                            self.registerObservers(forAddress: address, atIndexPath: indexPath)
                        }
                    })
                }
            }
        }
    }
    
    private func registerLongPressGestureRecognizers() {
        let recognizer = UILongPressGestureRecognizer(target: self, action: #selector(collectionViewLongPressed))
        addressesCollectionView.addGestureRecognizer(recognizer)
    }
    
    private func changeHeaderImage(to image: UIImage) {
        UIView.animate(withDuration: 0.5, animations: {
            self.highlightImageView.alpha = 0
        }) { (success) in
            guard success else { return }
            self.highlightImageView.image = image
            UIView.animate(withDuration: 0.5) {
                self.highlightImageView.alpha = 1
            }
        }
    }
    
    private func removeSelection() {
        guard !mAddressItems.nothingSelected else { return }
        for (index, item) in mAddressItems.enumerated() {
            if item.isSelected {
                item.isSelected = false
                continue
            }
            let indexPath = IndexPath(row: index, section: 0)
            registerObservers(forAddress: item.address, atIndexPath: indexPath)
        }
    }
    
    private func showDeletionAlert(for report: Report, _ completion: @escaping (Bool) -> ()) {
        let alertController = UIAlertController(title: NSLocalizedString("Are you sure?", comment: ""), message: NSLocalizedString("Do you really want to delete this report?", comment: ""), preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { (_) in
            completion(false)
        }))
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive, handler: { (_) in
            self.delete(report, completion)
        }))
        present(alertController, animated: true)
    }
    
    private func delete(_ report: Report, _ completion: @escaping (Bool) -> ()) {
        Database.database()
            .reference(withPath: "reports")
            .child(report.id)
            .removeValue(completionBlock: { (error, _) in
                guard error == nil else {
                    self.showError(error)
                    completion(false)
                    return
                }
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.updateApplicationContext()
                completion(true)
            })
    }
    
    func openReport(_ report: Report) {
        clickedReport = report
        performSegue(withIdentifier: "reportSegue", sender: nil)
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mAddressItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! AddressCollectionViewCell
        let item = mAddressItems[indexPath.row]
        if mShaking {
            cell.deleteButton.isHidden = false
            cell.imageButton.shake()
        } else {
            cell.isSelected = item.isSelected
            cell.deleteButton.isHidden = true
        }
        cell.address = item.address
        cell.addressButtonTapped = addressButtonTapped
        cell.deleteTapped = deleteTapped
        return cell
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        /*if let indexPath = addressesCollectionView.indexPathForItem(at: CGPoint(x: scrollView.contentOffset.x, y: addressesCollectionView.frame.height / 2)) {
            addressesCollectionView.scrollToItem(at: indexPath, at: .left, animated: true)
        }*/
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return mAddressItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mAddressItems[section].reports.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reportCell", for: indexPath)
        let report = mAddressItems[indexPath.section].reports[indexPath.row]
        cell.textLabel?.text = report.title
        cell.detailTextLabel?.text = report.type?.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let report = mAddressItems[indexPath.section].reports[indexPath.row]
        var actions = [UIContextualAction]()
        // delete action
        if report.userId == Auth.auth().currentUser?.uid { // only for own reports
            let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completion) in
                self.showDeletionAlert(for: report, completion)
            }
            deleteAction.image = UIImage(named: "delete")
            actions.append(deleteAction)
        }
        return UISwipeActionsConfiguration(actions: actions)
    }
}

extension Array where Element == AddressItem {
    var nothingSelected: Bool {
        for item in self {
            if item.isSelected {
                return false
            }
        }
        return true
    }
    
    func indexPath(ofReportWithId reportId: String) -> IndexPath? {
        for (section, item) in enumerated() {
            if let row = item.reports.firstIndex(where: { (report) -> Bool in report.id == reportId }) {
                return IndexPath(row: row, section: section)
            }
        }
        return nil
    }
}

extension Array where Element == Report {
    func indexPaths(usingSection section: Int) -> [IndexPath] {
        guard !isEmpty else { return [] }
        var indexPaths = [IndexPath]()
        indexPaths.reserveCapacity(count)
        for row in 0..<count {
            let indexPath = IndexPath(row: row, section: section)
            indexPaths.append(indexPath)
        }
        return indexPaths
    }
}

class AddressItem {
    var address: Address
    var query: Query?
    var isSelected = false
    var reports = [Report]()
    
    init(address: Address) {
        self.address = address
    }
}

struct Query {
    let query: GFCircleQuery
    let keyEnteredHandle: UInt
    let keyExitedHandle: UInt
    
    func removeObservers() {
        query.removeObserver(withFirebaseHandle: keyEnteredHandle)
        query.removeObserver(withFirebaseHandle: keyExitedHandle)
    }
}
