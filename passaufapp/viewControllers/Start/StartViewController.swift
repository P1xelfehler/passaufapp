//
//  AnimatedLaunchScreenViewController.swift
//  passaufapp
//
//  Created by Norman Laudien on 10.01.19.
//  Copyright © 2019 Omekron. All rights reserved.
//

import UIKit

class AnimatedLaunchScreenViewController: UIViewController {
    
    private let LOADING_TIME = 3.0
    private var mTimer: Timer!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var animationImageView: UIImageView!
    @IBOutlet private weak var versionLabel: UILabel!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadVersionName()
        loadAnimation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        startTimer()
    }
    
    // MARK: - Other methods
    
    private func loadVersionName() {
        let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        versionLabel.text = "Version: \(version)"
    }
    
    private func loadAnimation() {
        var images = [UIImage]()
        images.reserveCapacity(79)
        for i in 0..<78 {
            let image = UIImage(
                named: i <= 9 ? "warnradar_0000\(i)" : "warnradar_000\(i)"
            )!
            images.append(image)
        }
        animationImageView.animationImages = images
        animationImageView.startAnimating()
    }
    
    private func startTimer() {
        mTimer = Timer.scheduledTimer(
            timeInterval: LOADING_TIME,
            target: self,
            selector: #selector(finishLaunching),
            userInfo: nil,
            repeats: false
        )
    }
    
    @objc private func finishLaunching() {
        mTimer.invalidate()
        if UserDefaults.standard.bool(forKey: "EULA") { // EULA already accepted
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let mainVC = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()!
            appDelegate.window?.rootViewController = mainVC
        } else { // EULA not accepted yet
            performSegue(withIdentifier: "eulaSegue", sender: nil)
        }
    }
}
