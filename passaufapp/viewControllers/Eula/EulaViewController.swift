//
//  EulaViewController.swift
//  passaufapp
//
//  Created by Norman Laudien on 20.11.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import IntroScreen
import WebKit

class EulaViewController: IntroPageViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Outlets
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupWebView()
    }
    
    // MARK: - Actions
    
    @IBAction func acceptButtonTapped(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "EULA")
        dismiss(animated: true)
    }
    
    // MARK: - Other functions
    
    private func setupWebView() {
        let url = Bundle.main.url(forResource: "AGBs", withExtension: "html")!
        webView.navigationDelegate = self
        webView.isOpaque = false
        webView.loadFileURL(url, allowingReadAccessTo: url)
    }
}

extension EulaViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
    }
}
