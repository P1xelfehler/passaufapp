//
//  MainViewController.swift
//  passaufapp
//
//  Created by Norman Laudien on 06.12.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import UIKit
import Firebase
import FirebaseUI

class MainViewController: UITabBarController {
    private var handle: AuthStateDidChangeListenerHandle?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            if user == nil {
                if let authViewController = FUIAuth.defaultAuthUI()?.authViewController() {
                    authViewController.navigationBar.barStyle = .black
                    self.present(authViewController, animated: true)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Auth.auth().removeStateDidChangeListener(handle!)
    }
}
