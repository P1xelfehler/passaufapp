//
//  MapViewController.swift
//  passaufapp
//
//  Created by Norman Laudien on 01.11.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import DeepDiff
import Firebase
import GeoFire

class MapViewController: UIViewController {
    private let mLocationManager = CLLocationManager()
    override var prefersStatusBarHidden: Bool {
        return true
    }
    private var mAnnotations = [ReportAnnotation]()
    private var mGeoFire: GeoFire!
    private var mGeoFireQuery: GFRegionQuery?
    private var mKeyEnteredHandle: UInt?
    private var mKeyExitedHandle: UInt?
    private var mFilterVisible: Bool = false {
        didSet {
            if mFilterVisible { // show filter
                mapView.isUserInteractionEnabled = false
                searchButton.setImage(UIImage(named: "remove"), for: .normal)
                UIView.animate(withDuration: 0.3) {
                    self.filterView.transform = .identity
                    self.filterView.alpha = 0.8
                }
            } else { // hide filter
                mapView.isUserInteractionEnabled = true
                searchButton.setImage(UIImage(named: "filter"), for: .normal)
                UIView.animate(withDuration: 0.3, animations: {
                    self.filterView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
                    self.filterView.alpha = 0
                }) { (_) in
                    self.filterReports()
                }
            }
        }
    }
    private var premiumFeaturesEnabled = true {
        didSet {
            if premiumFeaturesEnabled { // looking premium
                searchButton.isHidden = false
                premiumDetailsButton.isHidden = true
                mapView.showsUserLocation = true
            } else { // looking freemium
                mFilterVisible = false
                searchButton.isHidden = true
                premiumDetailsButton.isHidden = false
                mapView.showsUserLocation = false
            }
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet var typeButtons: [SelectableButton]!
    @IBOutlet weak var premiumDetailsButton: UIButton!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        filterView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        filterView.alpha = 0
        locationButton.makeFloatingActionButton()
        searchButton.makeFloatingActionButton()
        let ref = Database.database().reference(withPath: "reports")
        mGeoFire = GeoFire(firebaseRef: ref)
        selectAllButtons()
        mLocationManager.delegate = self
        mapView.userTrackingMode = .follow
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mapView.mapType = UserDefaults.standard.bool(forKey: Setting.satelliteEnabled) ? .hybrid : .standard
        StoreUtils.isPremium { (isPremium) in
            self.premiumFeaturesEnabled = isPremium
            self.setupMapView()
            self.mLocationManager.requestWhenInUseAuthorization()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterDatabaseObservers()
        clear()
    }
    
    // MARK: - Actions
    
    @IBAction func locationButtonTapped(_ sender: UIButton) {
        if let coordinate = mLocationManager.location?.coordinate {
            let region = MKCoordinateRegion(center: coordinate, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            mapView.setRegion(region, animated: true)
        }
    }
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        mFilterVisible = !mFilterVisible
    }
    
    @IBAction func categoryButtonTapped(_ sender: SelectableButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func premiumDetailsButtonTapped(_ sender: UIButton) {
        let storeUtils = StoreUtils.shared
        registerStoreUtilsAlertHandlers(for: storeUtils)
        storeUtils.showBuyAlertController(in: self)
    }
    
    // MARK: - Segues
    
    @IBAction func unwindToMap(_ segue: UIStoryboardSegue) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "reportSegue":
            let destination = segue.destination.children[0] as! ReportViewController
            let report = sender as! Report
            destination.loadFromSegue(report: report, showDoneButton: true)
        default:
            break
        }
    }
    
    // MARK: - Other methods
    
    private func setupMapView() {
        if let lastLocation = mLocationManager.location {
            let region = MKCoordinateRegion(center: lastLocation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            if mGeoFireQuery == nil {
                registerDatabaseObservers(forRegion: region)
            }
        } else if mGeoFireQuery == nil {
            registerDatabaseObservers(forRegion: mapView.region)
        }
    }
    
    private func registerDatabaseObservers(forRegion region: MKCoordinateRegion) {
        mGeoFireQuery = mGeoFire.query(with: mapView.region)
        mKeyEnteredHandle = mGeoFireQuery!.observe(.keyEntered) { (reportId, location) in
            guard !self.mAnnotations.contains(where: { (annotation) -> Bool in
                annotation.report?.id == reportId
            }) else { return }
            self.fetchReport(withId: reportId, atLocation: location)
        }
        mKeyExitedHandle = mGeoFireQuery!.observe(.keyExited, with: { (reportId, location) in
            guard let index = self.mAnnotations.firstIndex(where: { (annotation) -> Bool in
                annotation.report?.id == reportId
            }) else { return }
            let annotation = self.mAnnotations[index]
            self.mapView.removeAnnotation(annotation)
            self.mAnnotations.remove(at: index)
        })
    }
    
    private func unregisterDatabaseObservers() {
        guard let query = mGeoFireQuery else { return }
        if let enteredHandle = mKeyEnteredHandle {
            query.removeObserver(withFirebaseHandle: enteredHandle)
        }
        if let exitedHandle = mKeyExitedHandle {
            query.removeObserver(withFirebaseHandle: exitedHandle)
        }
        mGeoFireQuery = nil
    }
    
    private func fetchReport(withId id: String, atLocation location: CLLocation) {
        Database.database()
            .reference(withPath: "reports")
            .child(id)
            .observeSingleEvent(of: .value) { (snapshot) in
                guard let value = snapshot.value as? [String: AnyObject] else { return }
                let report = Report(fromKey: snapshot.key, andValue: value, withLocation: location)
                guard self.premiumFeaturesEnabled || report.userId == Auth.auth().currentUser?.uid else { return }
                guard let type = report.type else { return }
                guard self.typeButtons.selectedReportTypes.contains(type) else { return }
                let annotation = ReportAnnotation()
                annotation.title = report.type?.title
                annotation.subtitle = report.message
                annotation.coordinate = report.location.coordinate
                annotation.report = report
                self.mAnnotations.append(annotation)
                self.mapView.addAnnotation(annotation)
        }
    }
    
    private func clear() {
        mAnnotations.removeAll()
        mapView.removeAnnotations(mapView.annotations)
    }
    
    private func selectAllButtons() {
        for button in typeButtons {
            button.isSelected = true
        }
    }
    
    private func filterReports() {
        unregisterDatabaseObservers()
        // remove reports that do not match the filter
        let selectedReportTypes = typeButtons.selectedReportTypes
        self.mAnnotations.removeAll { (annotation) -> Bool in
            if let reportType = annotation.report?.type, !selectedReportTypes.contains(reportType) {
                mapView.removeAnnotation(annotation)
                return true
            }
            return false
        }
        registerDatabaseObservers(forRegion: mapView.region)
    }
    
    private func openMaps(withReport report: Report) {
        let placemark = MKPlacemark(coordinate: report.location.coordinate)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = report.type?.title
        mapItem.openInMaps(launchOptions: nil)
    }
}

extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            break
        case .denied:
            let alertController = UIAlertController(title: NSLocalizedString("Location access needed", comment: ""), message: NSLocalizedString("The app needs location access for the map. Please enable this in settings.", comment: ""), preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Close", comment: ""), style: .cancel))
            let settingsAction = UIAlertAction(title: NSLocalizedString("Open settings", comment: ""), style: .default) { (_) in
                let url = URL(string: UIApplication.openSettingsURLString)!
                UIApplication.shared.open(url)
            }
            alertController.addAction(settingsAction)
            alertController.preferredAction = settingsAction
            present(alertController, animated: true)
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
        case .restricted:
            showAlert(title: NSLocalizedString("Location Restricted", comment: ""), message: NSLocalizedString("The location access is restriced for you. The map cannot use your location.", comment: ""))
        }
    }
}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let region = mapView.region
        guard region.span.latitudeDelta <= 10 && region.span.longitudeDelta <= 10 else { return }
        if let query = mGeoFireQuery {
            query.region = region
        } else {
            registerDatabaseObservers(forRegion: region)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is ReportAnnotation else { return nil }
        let annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: "loc")
        annotationView.canShowCallout = true
        annotationView.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        let navigationButton = UIButton()
        navigationButton.sizeToFit()
        navigationButton.setImage(UIImage(named: "navigation"), for: .normal)
        annotationView.leftCalloutAccessoryView = navigationButton
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let annotation = view.annotation as? ReportAnnotation else { return }
        guard let report = annotation.report else { return }
        guard let button = control as? UIButton else { return }
        if button.buttonType == .detailDisclosure {
            performSegue(withIdentifier: "reportSegue", sender: report)
        } else {
            openMaps(withReport: report)
        }
    }
}

// I love extensions ❤️
extension Dictionary where Key == String, Value == MKAnnotation {
    mutating func removeValues(forKeys keys: Dictionary<Key, Value>.Keys) {
        for key in keys {
            removeValue(forKey: key)
        }
    }
}

extension Array where Element == SelectableButton {
    var selectedReportTypes: [ReportType] {
        var selectedReportTypes = [ReportType]()
        for button in self {
            if button.isSelected {
                let reportType = ReportType(rawValue: button.tag)!
                selectedReportTypes.append(reportType)
            }
        }
        return selectedReportTypes
    }
}

class ReportAnnotation: MKPointAnnotation {
    var report: Report?
}
