//
//  WebViewController.swift
//  passaufapp
//
//  Created by Norman Laudien on 26.11.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    private var mHtmlName: String?
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Outlets
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let htmlName = mHtmlName {
            setupWebView(withFileName: htmlName)
        }
    }
    
    // MARK: - Actions
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    // MARK: - Segues
    
    func loadFromSegue(htmlName: String) {
        mHtmlName = htmlName
    }
    
    // MARK: - Other methods
    
    private func setupWebView(withFileName fileName: String) {
        let url = Bundle.main.url(forResource: fileName, withExtension: "html")!
        webView.navigationDelegate = self
        webView.isOpaque = false
        webView.loadFileURL(url, allowingReadAccessTo: url)
        webView.scrollView.indicatorStyle = .white
    }
}

extension WebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
    }
}
