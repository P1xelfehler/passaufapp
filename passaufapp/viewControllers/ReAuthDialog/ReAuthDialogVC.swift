//
//  ReAuthDialogVC.swift
//  leximaster
//
//  Created by Norman Laudien on 30.07.18.
//  Copyright © 2018 Norman Laudien. All rights reserved.
//

import UIKit
import FirebaseAuth
import GoogleSignIn

class ReAuthDialogVC: UIViewController {
    var successHandler: (() -> ())?
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var dialogViewHeight: NSLayoutConstraint!
    @IBOutlet weak var authButton: UIButton!
    @IBOutlet weak var googleButton: GIDSignInButton!
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func authenticate(_ sender: UIButton) {
        guard let user = Auth.auth().currentUser,
            let email = user.email, let password = passwordTextField.text, !password.isEmpty else {
                return
        }
        let credential = EmailAuthProvider.credential(withEmail: email, password: password)
        finishReAuthentication(forUser: user, withCredential: credential)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareLayout()
    }
    
    private func prepareLayout() {
        // google is the provider
        if let providerData = Auth.auth().currentUser?.providerData {
            for data in providerData {
                if data.providerID == GoogleAuthProviderID {
                    let googleInstance = GIDSignIn.sharedInstance()
                    googleInstance?.uiDelegate = self
                    googleInstance?.delegate = self
                    passwordTextField.isHidden = true
                    authButton.isHidden = true
                    dialogViewHeight.constant = 160
                    self.view.layoutIfNeeded()
                    break
                }
            }
        }
        // google is not the provider
        if !passwordTextField.isHidden {
            googleButton.isHidden = true
        }
    }
    
    private func finishReAuthentication(forUser user: User, withCredential credential: AuthCredential) {
        user.reauthenticateAndRetrieveData(with: credential) { (result, error) in
            guard error == nil else {
                self.showError(error!)
                return
            }
            self.dismiss(animated: true, completion: {
                self.successHandler?()
            })
        }
    }
    
    deinit {
        GIDSignIn.sharedInstance().delegate = UIApplication.shared.delegate as! AppDelegate
    }
}

extension ReAuthDialogVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false // do not add line break
    }
}

extension ReAuthDialogVC: GIDSignInUIDelegate, GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard error == nil, let authentication = user.authentication, let authUser = Auth.auth().currentUser else {
            self.showError(error)
            return
        }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
        finishReAuthentication(forUser: authUser, withCredential: credential)
    }
}
