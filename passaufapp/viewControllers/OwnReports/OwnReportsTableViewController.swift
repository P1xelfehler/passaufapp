//
//  OwnReportsTableViewController.swift
//  passaufapp
//
//  Created by Norman Laudien on 29.11.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import UIKit
import Firebase
import GeoFire

class OwnReportsTableViewController: UITableViewController {
    private var mReports = [Report]()
    private var mDatabaseReference: DatabaseQuery? {
        guard let userId = Auth.auth().currentUser?.uid else { return nil }
        return Database.database()
            .reference(withPath: "reports")
            .queryOrdered(byChild: "userId")
            .queryEqual(toValue: userId)
    }
    private var mChildAddedHandle: DatabaseHandle?
    private var mChildRemovedHandle: DatabaseHandle?
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerDatabaseObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterDatabaseObservers()
    }
    
    // MARK: - TableView
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mReports.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reportCell", for: indexPath)
        let report = mReports[indexPath.row]
        cell.textLabel?.text = report.title
        cell.detailTextLabel?.text = report.type?.title
        return cell
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let report = mReports[indexPath.row]
        let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completion) in
            self.showDeletionAlert(for: report, completion)
        }
        deleteAction.image = UIImage(named: "delete")
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "reportSegue":
            let destination = segue.destination as! ReportViewController
            let cell = sender as! UITableViewCell
            if let indexPath = tableView.indexPath(for: cell) {
                let report = mReports[indexPath.row]
                destination.loadFromSegue(report: report)
            }
        default:
            break
        }
    }
    
    // MARK: - Data fetching
    
    private func registerDatabaseObservers() {
        guard let ref = mDatabaseReference else { return }
        mChildAddedHandle = ref.observe(.childAdded) { (snapshot) in
            guard let value = snapshot.value as? [String: AnyObject] else { return }
            let geoRef = Database.database().reference(withPath: "reports")
            let geoFire = GeoFire(firebaseRef: geoRef)
            geoFire.getLocationForKey(snapshot.key, withCallback: { (location, _) in
                guard let location = location else { return }
                let report = Report(fromKey: snapshot.key, andValue: value, withLocation: location)
                guard !self.mReports.contains(report) else { return }
                let indexPath = IndexPath(row: self.mReports.count, section: 0)
                self.mReports.append(report)
                self.tableView.insertRows(at: [indexPath], with: .automatic)
            })
        }
        mChildRemovedHandle = ref.observe(.childRemoved) { (snapshot) in
            guard let _ = snapshot.value as? [String: AnyObject] else { return }
            guard let index = self.mReports.firstIndex(where: { (report) -> Bool in
                report.id == snapshot.key
            }) else { return }
            let indexPath = IndexPath(row: index, section: 0)
            self.mReports.remove(at: index)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    private func unregisterDatabaseObservers() {
        guard let ref = mDatabaseReference else { return }
        if let childAddedHandle = mChildAddedHandle {
            ref.removeObserver(withHandle: childAddedHandle)
        }
        if let childRemovedHandle = mChildRemovedHandle {
            ref.removeObserver(withHandle: childRemovedHandle)
        }
    }
    
    // MARK: - Other methods
    
    private func showDeletionAlert(for report: Report, _ completion: @escaping (Bool) -> ()) {
        let alertController = UIAlertController(title: NSLocalizedString("Are you sure?", comment: ""), message: NSLocalizedString("Do you really want to delete this report?", comment: ""), preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { (_) in
            completion(false)
        }))
        alertController.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (_) in
            self.delete(report, completion)
        }))
        present(alertController, animated: true)
    }
    
    private func delete(_ report: Report, _ completion: @escaping (Bool) -> ()) {
        Database.database()
            .reference(withPath: "reports")
            .child(report.id)
            .removeValue(completionBlock: { (error, _) in
                guard error == nil else {
                    self.showError(error)
                    completion(false)
                    return
                }
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.updateApplicationContext()
                completion(true)
            })
    }
}
