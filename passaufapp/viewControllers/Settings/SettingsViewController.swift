//
//  SettingsViewController.swift
//  passaufapp
//
//  Created by Norman Laudien on 01.11.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import UIKit
import StoreKit
import FirebaseAuth

class SettingsViewController: UITableViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    private var mAuthHandle: AuthStateDidChangeListenerHandle?
    private var mShowEmailVerificationView = false {
        didSet {
            UIView.animate(withDuration: 0.3) {
                if self.mShowEmailVerificationView {
                    self.tableView.tableHeaderView = self.emailVerificationView
                } else {
                    self.tableView.tableHeaderView = nil
                }
            }
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var satelliteSwitch: UISwitch!
    @IBOutlet private var emailVerificationView: UIView!
    
    @IBAction private func switchChanged(_ sender: UISwitch) {
        switch sender {
        case satelliteSwitch:
            UserDefaults.standard.set(sender.isOn, forKey: Setting.satelliteEnabled)
        default:
            break
        }
    }
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadSettings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerAuthStateDidChangeListener()
        StoreUtils.isPremium { (isPremium) in
            let cell = self.tableView(self.tableView, cellForRowAt: IndexPath(row: 8, section: 0))
            cell.isHidden = isPremium
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterAuthStateDidChangeListener()
    }
    
    // MARK: - Actions
    
    @IBAction func informationButtonTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "webViewSegue", sender: "Informationen")
    }
    
    @IBAction func helpButtonTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "webViewSegue", sender: "Hilfe")
    }
    
    @IBAction func dataProtectionButtonTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "webViewSegue", sender: "Datenschutz")
    }
    
    @IBAction func termsAndConditionsButtonTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "webViewSegue", sender: "Nutzungsbedingung")
    }
    
    @IBAction func imprintButtonTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "webViewSegue", sender: "Impressum")
    }
    
    @IBAction func buyButtonTapped(_ sender: UIButton) {
        let storeUtils = StoreUtils.shared
        registerStoreUtilsAlertHandlers(for: storeUtils)
        storeUtils.showBuyAlertController(in: self)
    }
    
    @IBAction func restoreButtonTapped(_ sender: UIButton) {
        let storeUtils = StoreUtils.shared
        registerStoreUtilsAlertHandlers(for: storeUtils)
        showLoadingView()
        storeUtils.restorePurchases()
    }
    
    @IBAction func sendAgainButtonTapped(_ sender: UIButton) {
        guard let user = Auth.auth().currentUser else { return }
        showLoadingView()
        user.sendEmailVerification { [weak self] (error) in
            guard let self = self else { return }
            self.hideLoadingView()
            guard error == nil else {
                self.showError(error)
                return
            }
            self.showAlert(
                title: NSLocalizedString("Email sent", comment: ""),
                message: NSLocalizedString("An email with a verification link was sent to your email address.", comment: "")
            )
        }
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "webViewSegue":
            let destination = segue.destination as! WebViewController
            destination.loadFromSegue(htmlName: sender as! String)
        default:
            break
        }
    }
    
    // MARK: - Other methods
    
    private func loadSettings() {
        let defaults = UserDefaults.standard
        satelliteSwitch.isOn = defaults.bool(forKey: Setting.satelliteEnabled)
    }
    
    private func registerAuthStateDidChangeListener() {
        mAuthHandle = Auth.auth().addStateDidChangeListener { [weak self] (_, user) in
            guard let user = user else { return }
            if user.isEmailVerified {
                self?.mShowEmailVerificationView = false
            } else { // email is not verified
                user.reload(completion: { (error) in // make sure the user is up to date
                    self?.mShowEmailVerificationView = !user.isEmailVerified
                })
            }
        }
    }
    
    private func unregisterAuthStateDidChangeListener() {
        if let handle = mAuthHandle {
            Auth.auth().removeStateDidChangeListener(handle)
        }
    }
}

extension SettingsViewController: SKProductsRequestDelegate {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        for product in response.products {
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(payment)
        }
    }
}

class Setting {
    static let satelliteEnabled = "satelliteEnabled"
}
