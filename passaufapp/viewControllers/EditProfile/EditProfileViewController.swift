//
//  EditProfileViewController.swift
//  passaufapp
//
//  Created by Norman Laudien on 29.11.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import UIKit
import Firebase

class EditProfileViewController: UIViewController {
    private let notificationEvents = [UIResponder.keyboardWillShowNotification,
                                      UIResponder.keyboardWillHideNotification]
    
    // MARK: - Outlets
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var firstPasswordTextField: UITextField!
    @IBOutlet weak var secondPasswordTextField: UITextField!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonTopConstraint: NSLayoutConstraint!
    
    // MARK: - Actions
    
    @IBAction func saveChangesButtonTapped(_ sender: UIButton) {
        saveChanges()
    }
    
    @IBAction func logoutButtonTapped(_ sender: UIButton) {
        let alertController = UIAlertController(title: NSLocalizedString("Are you sure?", comment: ""), message: NSLocalizedString("Do you really want to sign out?", comment: ""), preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Sign out", comment: ""), style: .destructive, handler: { (_) in
            let auth = Auth.auth()
            if let user = auth.currentUser {
                do {
                    TokenUtils.removeToken(of: user)
                    try auth.signOut()
                } catch {
                    self.showError(error)
                }
            }
        }))
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel))
        present(alertController, animated: true)
    }
    
    @objc private func keyboardWillChange(notification: Notification) {
        if notification.name == UIResponder.keyboardWillHideNotification {
            topConstraint.constant = 80
            buttonTopConstraint.constant = 50
        } else if let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            let totalHeight = view.frame.height
            let heightLeft = totalHeight - keyboardHeight
            let distance = (heightLeft - 257) / 2
            topConstraint.constant = distance
            buttonTopConstraint.constant = distance
        }
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let user = Auth.auth().currentUser {
            loadUser(user)
        }
        registerKeyboardObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        unregisterKeyboardObservers()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        hideKeyboard()
    }
    
    // MARK: - Other methods
    
    private func registerKeyboardObservers() {
        guard view.frame.height <= 568 else { return } // only iPhone SE size or smaller
        for name in notificationEvents {
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: name, object: nil)
        }
    }
    
    private func unregisterKeyboardObservers() {
        for name in notificationEvents {
            NotificationCenter.default.removeObserver(self, name: name, object: nil)
        }
    }
    
    private func loadUser(_ user: User) {
        emailTextField.text = user.email
    }
    
    private func hideKeyboard() {
        let responders: [UIResponder] = [emailTextField, firstPasswordTextField, secondPasswordTextField]
        for responder in responders {
            if responder.isFirstResponder {
                responder.resignFirstResponder()
                break
            }
        }
    }
    
    private func saveChanges() {
        guard let user = Auth.auth().currentUser else { return }
        guard let email = emailTextField.text, !email.isEmpty else {
            showError(withLocalisedDescription: NSLocalizedString("Please enter an email address.", comment: ""))
            return
        }
        guard let firstPassword = firstPasswordTextField.text, !firstPassword.isEmpty else {
            showError(withLocalisedDescription: NSLocalizedString("Please enter a password.", comment: ""))
            return
        }
        guard let secondPassword = secondPasswordTextField.text, !secondPassword.isEmpty else {
            showError(withLocalisedDescription: NSLocalizedString("Please repeat your password.", comment: ""))
            return
        }
        guard firstPassword == secondPassword else {
            showError(withLocalisedDescription: NSLocalizedString("The passwords do not match.", comment: ""))
            return
        }
        showLoadingView()
        if email == user.email {
            changePassword(ofUser: user, to: firstPassword)
        } else {
            changeEmail(ofUser: user, to: email)
        }
    }
    
    func reAuthenticate(user: User) {
        performSegue(withIdentifier: "reAuthSegue", sender: nil)
    }
    
    func changePassword(ofUser user: User, to password: String) {
        user.updatePassword(to: password) { (error) in
            if let error = error {
                let error = error as NSError
                self.hideLoadingView()
                if error.code == AuthErrorCode.requiresRecentLogin.rawValue {
                    self.reAuthenticate(user: user)
                } else {
                    self.showError(error)
                }
                return
            }
            self.hideLoadingView()
            self.showAlert(title: NSLocalizedString("Success", comment: ""), message: NSLocalizedString("Your password has been changed successfully.", comment: ""))
        }
    }
    
    func changeEmail(ofUser user: User, to email: String) {
        user.updateEmail(to: email) { (error) in
            if let error = error {
                let error = error as NSError
                self.hideLoadingView()
                if error.code == AuthErrorCode.requiresRecentLogin.rawValue {
                    self.reAuthenticate(user: user)
                } else {
                    self.showError(error)
                }
                return
            }
            self.hideLoadingView()
            self.showAlert(title: NSLocalizedString("Success", comment: ""), message: NSLocalizedString("Your email has been changed successfully.", comment: ""))
        }
    }
}

extension EditProfileViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTextField:
            firstPasswordTextField.becomeFirstResponder()
            return false
        case firstPasswordTextField:
            secondPasswordTextField.becomeFirstResponder()
            return false
        case secondPasswordTextField:
            textField.resignFirstResponder()
            saveChanges()
            return false
        default:
            return true
        }
    }
}
