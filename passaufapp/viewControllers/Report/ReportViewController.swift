//
//  ReportViewController.swift
//  passaufapp
//
//  Created by Norman Laudien on 26.11.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import UIKit
import Firebase
import GeoFire

class ReportViewController: UIViewController {
    private var mReport: Report?
    private var mEditMode = false
    private var mShowDoneButton = false
    private var mValidations = 0 {
        didSet {
            validationLabel.text = "\(mValidations) \(NSLocalizedString(mValidations == 1 ? "validation" : "validations", comment: ""))"
            if let report = mReport,
                let userId = Auth.auth().currentUser?.uid,
                (report.userId == userId||report.validations.contains(userId)) {
                validationButton.isEnabled = false
                validationButton.setTitle(NSLocalizedString("validated", comment: ""), for: .normal)
            } else {
                validationButton.isEnabled = true
                validationButton.setTitle(NSLocalizedString("validate", comment: ""), for: .normal)
            }
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionTextView: UITextView!
    @IBOutlet private weak var mapView: MKMapView!
    @IBOutlet private weak var locationButton: UIButton!
    @IBOutlet private weak var validationLabel: UILabel!
    @IBOutlet private weak var validationButton: UIButton!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationButton.makeFloatingActionButton()
        if let report = mReport {
            loadReport(report)
        }
        if !mShowDoneButton {
            navigationItem.rightBarButtonItem = nil
        }
        if mEditMode {
            let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonTapped))
            let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveButtonTapped))
            navigationItem.leftBarButtonItem = cancelButton
            navigationItem.rightBarButtonItem = saveButton
            descriptionTextView.isEditable = true
            validationLabel.isHidden = true
            validationButton.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mapView.mapType = UserDefaults.standard.bool(forKey: Setting.satelliteEnabled) ? .hybrid : .standard
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        descriptionTextView.resignFirstResponder()
    }
    
    // MARK: - Actions
    
    @IBAction func locationButtonTapped(_ sender: UIButton) {
        if let coordinate = mReport?.location.coordinate {
            let region = MKCoordinateRegion(center: coordinate, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            mapView.setRegion(region, animated: true)
        }
    }
    
    @IBAction func validationButtonTapped(_ sender: UIButton) {
        guard let userId = Auth.auth().currentUser?.uid else { return }
        guard mReport?.validations.contains(userId) == false else { return }
        guard let reportId = mReport?.id else { return }
        showLoadingView()
        Functions.functions().httpsCallable("validateReport").call(["reportId":reportId]) { [weak self] (_, error) in
            self?.hideLoadingView()
            guard error == nil else {
                self?.showError(error)
                return
            }
            self?.mReport?.validations.append(userId)
            self?.mValidations += 1
            self?.showAlert(
                title: NSLocalizedString("Success", comment: ""),
                message: NSLocalizedString("Report validated!", comment: "")
            )
        }
    }
    
    @objc func cancelButtonTapped() {
        performSegue(withIdentifier: "unwindToHomeSegue", sender: nil)
    }
    
    @objc func saveButtonTapped() {
        guard var report = mReport else { return }
        report.message = descriptionTextView.text
        let ref = Database.database()
            .reference(withPath: "reports")
        ref.child(report.id)
            .setValue(report.dictionary) { [weak self] (error, _) in
                guard error == nil else { self?.showError(error); return }
                let geoFire = GeoFire(firebaseRef: ref)
                geoFire.setLocation(report.location, forKey: report.id) { (error) in
                    guard error == nil else { self?.showError(error); return }
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.updateApplicationContext()
                    self?.performSegue(withIdentifier: "unwindToHomeSegue", sender: nil)
                }
        }
    }
    
    // MARK: - Segues
    
    func loadFromSegue(report: Report, editMode: Bool = false, showDoneButton: Bool = false) {
        mReport = report
        mEditMode = editMode
        mShowDoneButton = showDoneButton
    }
    
    // MARK: - Data loading
    
    private func loadReport(_ report: Report) {
        if let locality = report.locality {
            titleLabel.text = "\(report.type?.title ?? NSLocalizedString("Report", comment: "")) in \(locality)"
        } else {
            titleLabel.text = report.type?.title
        }
        descriptionTextView.text = report.message
        mValidations = report.validations.count
        let coordinate = report.location.coordinate
        let annotation = MKPointAnnotation()
        annotation.title = report.type?.title
        annotation.subtitle = report.message
        annotation.coordinate = coordinate
        mapView.region = MKCoordinateRegion(center: coordinate, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        mapView.addAnnotation(annotation)
    }
    
    // MARK: - Other methods
    
    private func openMaps(withReport report: Report) {
        let placemark = MKPlacemark(coordinate: report.location.coordinate)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = report.type?.title
        mapItem.openInMaps(launchOptions: nil)
    }
}

extension ReportViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: "loc")
        annotationView.canShowCallout = true
        let navigationButton = UIButton()
        navigationButton.sizeToFit()
        navigationButton.setImage(UIImage(named: "navigation"), for: .normal)
        annotationView.leftCalloutAccessoryView = navigationButton
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if let report = mReport {
            openMaps(withReport: report)
        }
    }
}
