//
//  SearchAddressViewController.swift
//  passaufapp
//
//  Created by Norman Laudien on 12.12.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class SearchAddressViewController: UIViewController {
    private var mCoordinate: CLLocationCoordinate2D?
    private var mCompletionHandler: ((CLLocationCoordinate2D, Int) -> ())?
    private var mAnnotation: MKPointAnnotation? {
        didSet {
            saveButton.isEnabled = mAnnotation != nil
        }
    }
    private var mCircle: MKCircle?
    private var mRadius: Float = 0.5 {
        didSet {
            radiusLabel.text = "\(mRadiusInMeters) m"
            if let coordinate = mAnnotation?.coordinate {
                addOrUpdateCircle(at: coordinate)
            }
            // vibrate
            if let feedbackGenerator = mImpactFeedbackGenerator, Int(mRadius * 10000) / 500 != Int(oldValue * 10000) / 500 {
                feedbackGenerator.impactOccurred()
            }
        }
    }
    private var mRadiusInMeters: Int {
        return Int(mRadius * 10000)
    }
    private var mImpactFeedbackGenerator: UIImpactFeedbackGenerator?
    private var mTimer: Timer?
    private var mInputRadius: Float?
    
    // MARK: - Outlets
    
    @IBOutlet private weak var radiusLabel: UILabel!
    @IBOutlet private weak var radiusSlider: UISlider!
    @IBOutlet private weak var mapView: MKMapView!
    @IBOutlet private weak var saveButton: UIBarButtonItem!
    
    // MARK: - Actions
    
    @objc private func mapTapped(sender: UITapGestureRecognizer) {
        guard sender.state == .ended else { return }
        let point = sender.location(in: mapView)
        let coordinate = mapView.convert(point, toCoordinateFrom: mapView)
        addOrUpdateAnnotation(atCoordinate: coordinate)
        addOrUpdateCircle(at: coordinate)
        updateMapRegion(withCenter: coordinate)
        mCoordinate = coordinate
    }
    
    @IBAction func saveButtonTapped(_ sender: UIBarButtonItem) {
        guard let coordinate = mAnnotation?.coordinate else { return }
        mCompletionHandler?(coordinate, mRadiusInMeters)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func radiusSliderChanged(_ sender: UISlider) {
        if mImpactFeedbackGenerator == nil {
            let feedbackGenerator = UIImpactFeedbackGenerator(style: .light)
            feedbackGenerator.prepare()
            mImpactFeedbackGenerator = feedbackGenerator
        }
        mRadius = sender.value
    }
    
    @IBAction func radiusSliderDidFinishEditing(_ sender: UISlider) {
        var value = 500 * round(sender.value * 20) // round to next 500
        if value == 0 { // minimum is 500 m
            value = 500
        }
        sender.value = value / 10000
        radiusSliderChanged(sender)
        mImpactFeedbackGenerator = nil
        if let coordinate = mCoordinate {
            updateMapRegion(withCenter: coordinate)
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTapRecognizerToMap()
        mapView.mapType = UserDefaults.standard.bool(forKey: Setting.satelliteEnabled) ? .hybrid : .standard
        if let radius = mInputRadius {
            radiusSlider.value = radius
            mRadius = radius
        }
        if let coordinate = mCoordinate {
            loadMapView(withCoordinate: coordinate)
        }
    }
    
    // MARK: - Segues
    
    func loadFromSegue(coordinate: CLLocationCoordinate2D, radius: Int? = nil, _ completion: @escaping (CLLocationCoordinate2D, Int) -> ()) {
        mCoordinate = coordinate
        mCompletionHandler = completion
        if let radius = radius {
            mInputRadius = Float(radius) / 10000
        }
    }
    
    // MARK: - Other methods
    
    private func loadMapView(withCoordinate coordinate: CLLocationCoordinate2D) {
        updateMapRegion(withCenter: coordinate)
        addOrUpdateAnnotation(atCoordinate: coordinate)
        addOrUpdateCircle(at: coordinate)
    }
    
    private func addTapRecognizerToMap() {
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(mapTapped))
        mapView.addGestureRecognizer(recognizer)
    }
    
    private func addOrUpdateAnnotation(atCoordinate coordinate: CLLocationCoordinate2D) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        if let oldAnnotation = mAnnotation {
            mapView.removeAnnotation(oldAnnotation)
        }
        mapView.addAnnotation(annotation)
        mAnnotation = annotation
    }
    
    private func addOrUpdateCircle(at coordinate: CLLocationCoordinate2D) {
        let circle = MKCircle(center: coordinate, radius: CLLocationDistance(mRadiusInMeters))
        if let oldCircle = mCircle {
            mapView.removeOverlay(oldCircle)
        }
        mapView.addOverlay(circle)
        mCircle = circle
    }
    
    private func updateMapRegion(withCenter coordinate: CLLocationCoordinate2D) {
        let distance = CLLocationDistance(exactly: Double(mRadiusInMeters) * 2.1)!
        let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: distance, longitudinalMeters: distance)
        mapView.setRegion(region, animated: true)
    }
}

extension SearchAddressViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let circleView = MKCircleRenderer(overlay: overlay)
        circleView.fillColor = UIColor(named: "Tint")!.withAlphaComponent(0.4)
        return circleView
    }
    
    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
        if let oldTimer = mTimer {
            oldTimer.invalidate()
        }
        mTimer = Timer.scheduledTimer(withTimeInterval: 4, repeats: false) { (timer) in
            guard let coordinate = self.mCoordinate else { return }
            self.updateMapRegion(withCenter: coordinate)
        }
    }
}
