//
//  AddAddressViewController.swift
//  passaufapp
//
//  Created by Norman Laudien on 29.11.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase
import GeoFire

class AddAddressViewController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    private var mSelectedButton: SelectableButton?
    private var mPlacemarks = [CLPlacemark]()
    private var mSelectedLocation: CLLocation?
    private var mSelectedRadius: Int? {
        didSet {
            if let selectedRadius = mSelectedRadius {
                radiusButton.setTitle("\(selectedRadius) m", for: .normal)
            } else {
                radiusButton.setTitle(nil, for: .normal)
            }
        }
    }
    private let mGeoCoder = CLGeocoder()
    private var mLocationManager: CLLocationManager? {
        didSet {
            guard let locationManager = mLocationManager else { return }
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var mapView: MKMapView!
    @IBOutlet private var selectionView: UIView!
    @IBOutlet private var manualSelectionView: UIView!
    @IBOutlet private var selectedAddressView: UIView!
    @IBOutlet private weak var radiusButton: UIButton!
    
    // MARK: - Actions
    
    @IBAction private func saveButtonTapped(_ sender: UIBarButtonItem) {
        guard let location = mSelectedLocation, let radius = mSelectedRadius else {
            showError(withLocalisedDescription: NSLocalizedString("Please select an address.", comment: ""))
            return
        }
        guard let userId = Auth.auth().currentUser?.uid else { return }
        guard let id = Database.database()
            .reference(withPath: "users")
            .child(userId)
            .child("addresses")
            .childByAutoId()
            .key else { return }
        var address: Address!
        self.showLoadingView()
        if let selectedButton = mSelectedButton {
            let icon = AddressIcon(rawValue: selectedButton.tag)!
            address = Address(
                id: id,
                radius: radius,
                icon: icon,
                location: location
            )
            saveAddress(address, ofUserWithId: userId)
        } else {
            mGeoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
                guard let placemark = placemarks?.first, let locality = placemark.locality, locality.count >= 2 else {
                    self.hideLoadingView()
                    self.showError(nil)
                    return
                }
                let shorty = "\(locality[locality.startIndex])\(locality[locality.index(after: locality.startIndex)])".uppercased()
                address = Address(
                    id: id,
                    radius: radius,
                    shorty: shorty,
                    location: location
                )
                self.saveAddress(address, ofUserWithId: userId)
            }
        }
    }
    
    @IBAction private func iconButtonTapped(_ sender: SelectableButton) {
        if sender.isSelected { // deselect
            sender.isSelected = false
            mSelectedButton = nil
        } else { // select
            sender.isSelected = true
            if let selectedButton = mSelectedButton, sender != selectedButton {
                selectedButton.isSelected = false
            }
            mSelectedButton = sender
        }
    }
    
    @IBAction private func currentLocationTapped(_ sender: UIButton) {
        let status = CLLocationManager.authorizationStatus()
        handleLocationAccess(status: status)
    }
    
    @IBAction private func manualSelectionTapped(_ sender: UIButton) {
        replaceViewInContainer(with: manualSelectionView)
    }
    
    @IBAction private func changeButtonTapped(_ sender: UIButton) {
        mSelectedLocation = nil
        mSelectedRadius = nil
        replaceViewInContainer(with: selectionView)
    }
    
    @IBAction func searchViewCancelButtonTapped(_ sender: UIButton) {
        replaceViewInContainer(with: selectionView)
    }
    
    @IBAction func radiusButtonTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "searchAddressSegue", sender: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if let selectedButton = mSelectedButton { // remove selection
            selectedButton.isSelected = false
            mSelectedButton = nil
        }
    }
    
    private func didSelectAddress(atCoordinate coordinate: CLLocationCoordinate2D, withRadius radius: Int) {
        let distance = CLLocationDistance(exactly: Double(radius) * 2.1)!
        let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: distance, longitudinalMeters: distance)
        let circle = MKCircle(center: coordinate, radius: CLLocationDistance(radius))
        self.mSelectedLocation = CLLocation(coordinate: coordinate)
        self.mSelectedRadius = radius
        self.mapView.setRegion(region, animated: true)
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        self.mapView.removeAnnotations(self.mapView.annotations)
        self.mapView.removeOverlays(self.mapView.overlays)
        self.mapView.addAnnotation(annotation)
        self.mapView.addOverlay(circle)
        self.replaceViewInContainer(with: self.selectedAddressView, animate: false)
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "searchAddressSegueFromCell":
            let destination = segue.destination as! SearchAddressViewController
            let cell = sender as! UITableViewCell
            guard let indexPath = tableView.indexPath(for: cell) else { break }
            guard let coordinate = mPlacemarks[indexPath.row].location?.coordinate else { break }
            destination.loadFromSegue(coordinate: coordinate, didSelectAddress(atCoordinate:withRadius:))
        case "searchAddressSegue":
            hideLoadingView()
            guard let coordinate = mSelectedLocation?.coordinate else { break }
            let destination = segue.destination as! SearchAddressViewController
            destination.loadFromSegue(coordinate: coordinate, radius: mSelectedRadius, didSelectAddress(atCoordinate:withRadius:))
        default:
            break
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        replaceViewInContainer(with: selectionView)
        mapView.mapType = UserDefaults.standard.bool(forKey: Setting.satelliteEnabled) ? .hybrid : .standard
    }
    
    // MARK: - Other methods
    
    private func replaceViewInContainer(with view: UIView, animate: Bool = true) {
        view.frame = containerView.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        if let oldView = containerView.subviews.first {
            if animate {
                UIView.transition(from: oldView, to: view, duration: 0.3, options: .transitionFlipFromLeft)
            } else {
                oldView.removeFromSuperview()
                containerView.addSubview(view)
            }
        } else {
            containerView.addSubview(view)
        }
    }
    
    private func saveAddress(_ address: Address, ofUserWithId userId: String) {
        let geoRef = Database.database()
            .reference(withPath: "users")
            .child(userId)
            .child("addresses")
        let ref = geoRef.child(address.id)
        ref.setValue(address.dictionary) { (error, _) in
            guard error == nil else {
                self.hideLoadingView()
                self.showError(error)
                return
            }
            let geoFire = GeoFire(firebaseRef: geoRef)
            geoFire.setLocation(address.location, forKey: address.id, withCompletionBlock: { (error) in
                guard error == nil else {
                    self.hideLoadingView()
                    self.showError(error)
                    return
                }
                self.dismiss(animated: true)
            })
        }
    }
    
    private func handleLocationAccess(status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            showLoadingView()
            if mLocationManager == nil {
                mLocationManager = CLLocationManager()
            }
            mLocationManager!.requestLocation()
        case .denied:
            let alertController = UIAlertController(title: NSLocalizedString("Location access needed", comment: ""), message: NSLocalizedString("The app needs location access to use your position. Please enable this in settings.", comment: ""), preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Close", comment: ""), style: .cancel))
            let settingsAction = UIAlertAction(title: NSLocalizedString("Open settings", comment: ""), style: .default) { (_) in
                let url = URL(string: UIApplication.openSettingsURLString)!
                UIApplication.shared.open(url)
            }
            alertController.addAction(settingsAction)
            alertController.preferredAction = settingsAction
            present(alertController, animated: true)
        case .notDetermined:
            if mLocationManager == nil {
                mLocationManager = CLLocationManager()
            }
            mLocationManager!.requestWhenInUseAuthorization()
        case .restricted:
            showAlert(title: NSLocalizedString("Location Restricted", comment: ""), message: NSLocalizedString("The location access is restriced for you.", comment: ""))
        }
    }
}

extension AddAddressViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mPlacemarks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let placemark = mPlacemarks[indexPath.row]
        cell.textLabel?.text = placemark.locality
        cell.detailTextLabel?.text = placemark.country
        return cell
    }
}

extension AddAddressViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        guard let searchText = searchBar.text else { mPlacemarks.removeAll(); tableView.reloadData(); return }
        showLoadingView()
        mGeoCoder.geocodeAddressString(searchText) { (placemarks, error) in
            self.hideLoadingView()
            if let placemarks = placemarks {
                self.mPlacemarks = placemarks
            } else {
                self.mPlacemarks.removeAll()
            }
            self.tableView.reloadData()
        }
    }
}

extension AddAddressViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}

extension AddAddressViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let circleView = MKCircleRenderer(overlay: overlay)
        circleView.fillColor = UIColor(named: "Tint")!.withAlphaComponent(0.4)
        return circleView
    }
}

extension AddAddressViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        hideLoadingView()
        guard let location = locations.first else { return }
        mSelectedLocation = location
        if navigationController?.children.count == 1 {
            performSegue(withIdentifier: "searchAddressSegue", sender: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        handleLocationAccess(status: status)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        hideLoadingView()
        showError(error)
    }
}
