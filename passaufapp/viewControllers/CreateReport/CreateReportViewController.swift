//
//  CreateReportViewController.swift
//  passaufapp
//
//  Created by Norman Laudien on 05.11.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase

class CreateReportViewController: UIViewController {
    private let notificationEvents = [UIResponder.keyboardWillShowNotification,
                                      UIResponder.keyboardWillHideNotification]
    private var mLocationManager = CLLocationManager()
    private var mPlacemark: CLPlacemark?
    private var mLocation: CLLocation?
    private var mSelectedTypeButton: SelectableButton? {
        didSet {
            if let button = mSelectedTypeButton {
                let reportType = ReportType(rawValue: button.tag)!
                typeLabel.text = reportType.title
            } else {
                typeLabel.text = " "
            }
        }
    }
    private var mReport: Report?
    private var mLoadingAddress = true {
        didSet {
            if mLoadingAddress {
                addressTextField.isHidden = true
                zipTextField.isHidden = true
                cityTextField.isHidden = true
                refreshButton.isHidden = true
                addressActivityIndicator.startAnimating()
            } else {
                addressTextField.isHidden = false
                zipTextField.isHidden = false
                cityTextField.isHidden = false
                refreshButton.isHidden = false
                addressActivityIndicator.stopAnimating()
            }
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Outlets
    
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var zipTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var keyboardDownButton: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var addressActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var refreshButton: UIButton!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        keyboardDownButton.makeFloatingActionButton()
        registerKeyboardObservers()
        addressTextField.delegate = self
        zipTextField.delegate = self
        cityTextField.delegate = self
        mLocationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        mLocationManager.delegate = self
        mLocationManager.requestWhenInUseAuthorization()
    }
    
    deinit {
        unregisterKeyboardObservers()
    }
    
    // MARK: - Actions
    
    @IBAction func keyboardDownButtonTapped(_ sender: UIButton) {
        let views: [UIView] = [addressTextField, zipTextField, cityTextField, descriptionTextView]
        for view in views {
            if view.isFirstResponder {
                view.resignFirstResponder()
                return
            }
        }
    }
    
    @IBAction func refreshButtonTapped(_ sender: UIButton) {
        handleLocationAuthStatus(CLLocationManager.authorizationStatus())
    }
    
    @IBAction func doneButtonTapped(_ sender: UIBarButtonItem) {
        guard let placemark = mPlacemark, let location = mLocation else {
            showError(withLocalisedDescription: NSLocalizedString("Your location could not be determinated. Make sure, your location services are turned on and the app has location permissions.", comment: ""))
            return
        }
        guard let typeButton = mSelectedTypeButton else {
            showError(withLocalisedDescription: NSLocalizedString("Please select a report type.", comment: ""))
            return
        }
        guard let id = Database.database().reference(withPath: "reports").childByAutoId().key else {
            showError(nil)
            return
        }
        guard let userId = Auth.auth().currentUser?.uid else { return }
        mReport = Report(
            id: id,
            userId: userId,
            type: ReportType(rawValue: typeButton.tag)!,
            message: descriptionTextView.text,
            thoroughfare: placemark.thoroughfare,
            subThoroughfare: placemark.subThoroughfare,
            locality: placemark.locality,
            postalCode: placemark.postalCode == nil ? nil : Int(placemark.postalCode!),
            location: location
        )
        performSegue(withIdentifier: "reportSegue", sender: nil)
    }
    
    @IBAction func typeButtonTapped(_ sender: SelectableButton) {
        if sender.isSelected { // deselect
            sender.isSelected = false
            mSelectedTypeButton = nil
        } else { // select
            sender.isSelected = true
            if let selectedButton = mSelectedTypeButton, sender != selectedButton {
                selectedButton.isSelected = false
            }
            mSelectedTypeButton = sender
        }
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "reportSegue":
            if let report = mReport {
                let destination = segue.destination.children[0] as! ReportViewController
                destination.loadFromSegue(report: report, editMode: true)
            }
        default:
            break
        }
    }
    
    // MARK: - Keyboard
    
    private func registerKeyboardObservers() {
        for name in notificationEvents {
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: name, object: nil)
        }
    }
    
    private func unregisterKeyboardObservers() {
        for name in notificationEvents {
            NotificationCenter.default.removeObserver(self, name: name, object: nil)
        }
    }
    
    @objc private func keyboardWillChange(notification: Notification) {
        if notification.name == UIResponder.keyboardWillHideNotification {
            topConstraint.constant = 0
            bottomConstraint.constant = 10
            keyboardDownButton.isHidden = true
        } else if var keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            if let bottomInset = UIApplication.shared.keyWindow?.safeAreaInsets.bottom { // iPhone X like devices
                keyboardHeight -= bottomInset
            }
            topConstraint.constant = -keyboardHeight - 50
            bottomConstraint.constant = keyboardHeight + 10
            keyboardDownButton.isHidden = false
        }
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - Other methods
    
    private func handleLocationAuthStatus(_ status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            mLoadingAddress = true
            mLocationManager.requestLocation()
        case .denied:
            let alertController = UIAlertController(title: NSLocalizedString("Location access needed", comment: ""), message: NSLocalizedString("The app needs location access to create a report. Please enable this in settings.", comment: ""), preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Close", comment: ""), style: .cancel, handler: { (_) in
                self.dismiss(animated: true)
            }))
            let settingsAction = UIAlertAction(title: NSLocalizedString("Open settings", comment: ""), style: .default) { (_) in
                let url = URL(string: UIApplication.openSettingsURLString)!
                UIApplication.shared.open(url)
            }
            alertController.addAction(settingsAction)
            alertController.preferredAction = settingsAction
            present(alertController, animated: true)
        case .notDetermined:
            mLocationManager.requestWhenInUseAuthorization()
        case .restricted:
            showAlert(title: NSLocalizedString("Location Restricted", comment: ""), message: NSLocalizedString("The location access is restriced for you. You cannot create a report.", comment: "")) { (_) in
                self.dismiss(animated: true)
            }
        }
    }
    
    private func loadLocation(_ location: CLLocation) {
        let geoCoder = CLGeocoder()
        mLocation = location
        geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
            self.mLoadingAddress = false
            guard let placemark = placemarks?[0] else { return }
            self.mPlacemark = placemark
            self.cityTextField.text = placemark.locality
            self.zipTextField.text = placemark.postalCode
            if let thoroughfare = placemark.thoroughfare, let subThoroughfare = placemark.subThoroughfare {
                self.addressTextField.text = "\(thoroughfare) \(subThoroughfare)"
            } else {
                self.addressTextField.text = nil
            }
        }
    }
}

extension CreateReportViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension CreateReportViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            mLoadingAddress = false
            return
        }
        loadLocation(location)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        handleLocationAuthStatus(status)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        mLoadingAddress = false
        showError(error)
    }
}
