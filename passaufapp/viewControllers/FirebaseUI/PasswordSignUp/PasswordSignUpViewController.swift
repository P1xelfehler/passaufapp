//
//  PasswordSignUpViewController.swift
//  passaufapp
//
//  Created by Norman Laudien on 11.01.19.
//  Copyright © 2019 Omekron. All rights reserved.
//

import UIKit
import FirebaseUI

class PasswordSignUpViewController: FUIPasswordSignUpViewController {
    private var mIncomingEmail: String?
    
    // MARK: - Outlets
    
    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var nameTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    
    // MARK: - Inits
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, authUI: FUIAuth, email: String?) {
        super.init(nibName: "PasswordSignUp", bundle: nil, authUI: authUI, email: email)
        mIncomingEmail = email
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem?.action = #selector(nextButtonTapped)
        emailTextField.text = mIncomingEmail
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let email = mIncomingEmail {
            mIncomingEmail = nil
            emailTextField.text = email
        }
        didChangeEmail(
            emailTextField.text ?? "",
            orPassword: passwordTextField.text ?? "",
            orUserName: nameTextField.text ?? ""
        )
    }
    
    // MARK: - Actions
    
    @IBAction private func textfieldDidChange(_ sender: UITextField) {
        didChangeEmail(
            emailTextField.text ?? "",
            orPassword: passwordTextField.text ?? "",
            orUserName: nameTextField.text ?? ""
        )
    }
    
    @objc private func nextButtonTapped() {
        signUp(
            withEmail: emailTextField.text ?? "",
            andPassword: passwordTextField.text ?? "",
            andUsername: nameTextField.text ?? ""
        )
    }
}

extension PasswordSignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == passwordTextField {
            signUp(
                withEmail: emailTextField.text ?? "",
                andPassword: passwordTextField.text ?? "",
                andUsername: nameTextField.text ?? ""
            )
        } else if textField == emailTextField {
            nameTextField.becomeFirstResponder()
        } else { // must be name text field
            passwordTextField.becomeFirstResponder()
        }
        return true
    }
}
