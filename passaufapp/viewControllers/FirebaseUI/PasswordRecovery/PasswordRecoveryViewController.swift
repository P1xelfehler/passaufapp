//
//  PasswordRecovery.swift
//  passaufapp
//
//  Created by Norman Laudien on 11.01.19.
//  Copyright © 2019 Omekron. All rights reserved.
//

import UIKit
import FirebaseUI

class PasswordRecoveryViewController: FUIPasswordRecoveryViewController {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var emailTextField: UITextField!
    
    // MARK: - Inits
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, authUI: FUIAuth, email: String?) {
        super.init(nibName: "PasswordRecovery", bundle: nil, authUI: authUI, email: email)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem?.action = #selector(nextButtonTapped)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        didChangeEmail(emailTextField.text ?? "")
    }
    
    // MARK: - Actions
    
    @IBAction private func textfieldDidChange(_ sender: UITextField) {
        didChangeEmail(sender.text ?? "")
    }
    
    @objc private func nextButtonTapped() {
        recoverEmail(emailTextField.text ?? "")
    }
}

extension PasswordRecoveryViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        recoverEmail(emailTextField.text ?? "")
        return true
    }
}
