//
//  EmailEntryViewController.swift
//  passaufapp
//
//  Created by Norman Laudien on 11.01.19.
//  Copyright © 2019 Omekron. All rights reserved.
//

import UIKit
import FirebaseUI

class EmailEntryViewController: FUIEmailEntryViewController {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var emailTextField: UITextField!
    
    // MARK: - Inits
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, authUI: FUIAuth) {
        super.init(nibName: "EmailEntry", bundle: Bundle(for: FUIEmailEntryViewController.self), authUI: authUI)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem?.action = #selector(nextButtonTapped)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        didChangeEmail(emailTextField.text ?? "")
    }
    
    // MARK: - Actions
    
    @IBAction private func textfieldDidChange(_ sender: UITextField) {
        didChangeEmail(sender.text ?? "")
    }
    
    @objc private func nextButtonTapped() {
        onNext(emailTextField.text ?? "")
    }
}

extension EmailEntryViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        onNext(textField.text ?? "")
        return true
    }
}
