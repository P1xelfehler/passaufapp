//
//  PasswordSignInViewController.swift
//  passaufapp
//
//  Created by Norman Laudien on 11.01.19.
//  Copyright © 2019 Omekron. All rights reserved.
//

import UIKit
import FirebaseUI

class PasswordSignInViewController: FUIPasswordSignInViewController {
    private var mIncomingEmail: String?
    
    // MARK: - Outlets
    
    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    
    // MARK: - Inits
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, authUI: FUIAuth, email: String?) {
        super.init(nibName: "PasswordSignIn", bundle: nil, authUI: authUI, email: email)
        mIncomingEmail = email
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem?.action = #selector(nextButtonTapped)
        emailTextField.text = mIncomingEmail
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let email = mIncomingEmail {
            mIncomingEmail = nil
            emailTextField.text = email
        }
        didChangeEmail(
            emailTextField.text ?? "",
            andPassword: passwordTextField.text ?? ""
        )
    }
    
    // MARK: - Actions
    
    @IBAction private func textfieldDidChange(_ sender: UITextField) {
        didChangeEmail(
            emailTextField.text ?? "",
            andPassword: passwordTextField.text ?? ""
        )
    }
    
    @IBAction func resetPasswordTapped(_ sender: UIButton) {
        forgotPassword(forEmail: emailTextField.text ?? "")
    }
    
    @objc private func nextButtonTapped() {
        signIn(
            withDefaultValue: emailTextField.text ?? "",
            andPassword: passwordTextField.text ?? ""
        )
    }
}

extension PasswordSignInViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == passwordTextField {
            signIn(
                withDefaultValue: emailTextField.text ?? "",
                andPassword: textField.text ?? ""
            )
        } else { // must be email text field
            passwordTextField.becomeFirstResponder()
        }
        return true
    }
}
