//
//  AuthPickerVC.swift
//  leximaster
//
//  Created by Norman Laudien on 09.06.18.
//  Copyright © 2018 Norman Laudien. All rights reserved.
//

import UIKit
import FirebaseUI

class AuthPickerVC: FUIAuthPickerViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = nil
    }
}
