//
//  SelectableButton.swift
//  passaufapp
//
//  Created by Norman Laudien on 26.11.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import UIKit

class SelectableButton: UIButton {
    override var isSelected: Bool {
        didSet {
            backgroundColor = isSelected ? UIColor(named: "ButtonBackground") : nil
        }
    }
}
