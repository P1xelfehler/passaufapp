//
//  AddressCollectionViewCell.swift
//  passaufapp
//
//  Created by Norman Laudien on 02.11.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import UIKit

class AddressCollectionViewCell: UICollectionViewCell {
    var deleteTapped: ((AddressCollectionViewCell) -> ())?
    var addressButtonTapped: ((AddressCollectionViewCell) -> ())?
    
    var address: Address? {
        didSet {
            guard let address = address else { return }
            if let image = address.icon?.image {
                imageButton.setImage(image, for: .normal)
                imageButton.setTitle(nil, for: .normal)
            } else {
                imageButton.setImage(nil, for: .normal)
                imageButton.setTitle(address.shorty, for: .normal)
            }
        }
    }
    override var isSelected: Bool {
        didSet {
            backgroundColor = isSelected ? UIColor(named: "ButtonBackground") : .clear
        }
    }
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var imageButton: UIButton!
    
    @IBAction private func imageButtonTapped(_ sender: UIButton) {
        addressButtonTapped?(self)
    }
    
    @IBAction private func deleteButtonTapped(_ sender: UIButton) {
        deleteTapped?(self)
    }
}
