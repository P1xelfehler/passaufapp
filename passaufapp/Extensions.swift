//
//  Extensions.swift
//  passaufapp
//
//  Created by Norman Laudien on 19.11.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase

extension UIViewController {
    func showLoadingView() {
        let loadingView = LoadingView(frame: view.bounds)
        view.addSubview(loadingView)
    }
    
    func hideLoadingView() {
        if let loadingView = view.subviews.last as? LoadingView {
            loadingView.removeFromSuperview()
        }
    }
    
    func showError(_ error: Error?, handler: ((UIAlertAction) -> ())? = nil) {
        let errorMessage = error?.localizedDescription ?? "Unknown error"
        showError(withLocalisedDescription: NSLocalizedString(errorMessage, comment: ""), handler: handler)
    }
    
    func showError(withLocalisedDescription errorMessage: String, handler: ((UIAlertAction) -> ())? = nil) {
        let alertController = UIAlertController(
            title: NSLocalizedString("Error", comment: ""),
            message: errorMessage,
            preferredStyle: .alert
        )
        alertController.addAction(UIAlertAction(
            title: NSLocalizedString("Close", comment: ""),
            style: .default,
            handler: handler
        ))
        present(alertController, animated: true)
    }
    
    func showAlert(title: String, message: String, handler: ((UIAlertAction) -> ())? = nil){
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        alertController.addAction(UIAlertAction(
            title: NSLocalizedString("OK", comment: ""),
            style: .default,
            handler: handler
        ))
        present(alertController, animated: true)
    }
    
    func registerStoreUtilsAlertHandlers(for storeUtils: StoreUtils) {
        storeUtils.fetchAvailableProducts()
        storeUtils.alertHandler = { [weak self] (alert) in
            self?.showAlert(title: alert.title, message: alert.message)
            self?.hideLoadingView()
        }
        storeUtils.errorHandler = { [weak self] (error) in
            self?.showAlert(title: StoreUtilsAlertType.purchaseFailed.title, message: error.localizedDescription)
            self?.hideLoadingView()
        }
    }
}

extension UIButton {
    func makeFloatingActionButton() {
        layer.cornerRadius = frame.height / 2
        layer.shadowOpacity = 0.25
        layer.shadowRadius = 4
        layer.shadowOffset = CGSize(width: 0, height: 8)
    }
}

extension CLLocation {
    convenience init(coordinate: CLLocationCoordinate2D) {
        self.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }
}

extension UIView {
    func shake() {
        let animation = CABasicAnimation(keyPath: "transform")
        animation.duration = 0.125
        animation.repeatCount = .infinity
        animation.autoreverses = true
        animation.fromValue = CATransform3DMakeRotation(0.06, 0, 0, 1)
        animation.toValue = CATransform3DMakeRotation(-0.06, 0, 0, 1)
        layer.add(animation, forKey: "transform")
    }
}
