//
//  ReportInterfaceController.swift
//  watch Extension
//
//  Created by Norman Laudien on 20.12.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import WatchKit
import Foundation


class ReportInterfaceController: WKInterfaceController {
    private var mReport: Report? {
        didSet {
            guard let report = mReport else { return }
            if let locality = report.locality {
                titleLabel.setText("\(report.type?.title ?? NSLocalizedString("Report", comment: "")) in \(locality)")
            } else {
                titleLabel.setText(report.type?.title)
            }
            messageLabel.setText(report.message)
            let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
            let region = MKCoordinateRegion(center: report.location.coordinate, span: span)
            map.setRegion(region)
            map.addAnnotation(report.location.coordinate, with: .red)
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var titleLabel: WKInterfaceLabel!
    @IBOutlet private weak var messageLabel: WKInterfaceLabel!
    @IBOutlet private weak var map: WKInterfaceMap!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        if let report = context as? Report {
            mReport = report
        }
    }
}
