//
//  DatabaseController.swift
//  watch Extension
//
//  Created by Norman Laudien on 20.12.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import Foundation
import CoreLocation

class DatabaseController {
    static let firebaseParams = NSDictionary(contentsOfFile: Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")!)!
    private static let DATABASE_URL = firebaseParams["DATABASE_URL"] as! String
    private static let API_KEY = firebaseParams["API_KEY"] as! String
    
    static func fetchReports(_ completion: @escaping ([Report]?, Error?) -> ()) {
        func completeInMainThread(reports: [Report]?, error: Error?) {
            DispatchQueue.main.async {
                completion(reports, error)
            }
        }
        let url = URL(string: "\(DATABASE_URL)/reports.json")!
        let task = URLSession.shared.dataTask(with: url) { (data, _, error) in
            guard let data = data else { completeInMainThread(reports: nil, error: error); return }
            do {
                guard let json = try JSONSerialization.jsonObject(with: data) as? [String: Any] else { return }
                var reports = [Report]()
                for (key, value) in json {
                    guard let value = value as? [String: AnyObject] else { return }
                    guard let loc = value["l"] as? [Double] else { return }
                    guard loc.count == 2 else { return }
                    let location = CLLocation(latitude: loc[0], longitude: loc[1])
                    let report = Report(
                        fromKey: key,
                        andValue: value,
                        withLocation: location
                    )
                    reports.append(report)
                }
                completeInMainThread(reports: reports, error: error)
            } catch {
                completeInMainThread(reports: nil, error: error)
            }
        }
        task.resume()
    }
    
    static func fetchReport(withId reportId: String, _ completion: @escaping (Report?, Error?) -> ()) {
        func completeInMainThread(report: Report?, error: Error?) {
            DispatchQueue.main.async {
                completion(report, error)
            }
        }
        let url = URL(string: "\(DATABASE_URL)/reports/\(reportId).json")!
        let task = URLSession.shared.dataTask(with: url) { (data, _, error) in
            guard let data = data else { completeInMainThread(report: nil, error: error); return }
            do {
                guard let json = try JSONSerialization.jsonObject(with: data) as? [String: AnyObject] else { return }
                guard let loc = json["l"] as? [Double] else { return }
                guard loc.count == 2 else { return }
                let location = CLLocation(latitude: loc[0], longitude: loc[1])
                let report = Report(
                    fromKey: reportId,
                    andValue: json,
                    withLocation: location
                )
                completeInMainThread(report: report, error: error)
            } catch {
                completeInMainThread(report: nil, error: error)
            }
        }
        task.resume()
    }
}

struct DecodableReport: Decodable {
    let userId: String?
    let type: ReportType?
    var message: String?
    let thoroughfare: String?
    let subThoroughfare: String?
    let locality: String?
    let postalCode: Int?
    let l: [Double]
    
    func getReport(usingId reportId: String) -> Report? {
        guard l.count == 2 else { return nil }
        let location = CLLocation(latitude: l[0], longitude: l[1])
        return Report(id: reportId, userId: userId, type: type, message: message, thoroughfare: thoroughfare, subThoroughfare: subThoroughfare, locality: locality, postalCode: postalCode, location: location)
    }
}
