//
//  ExtensionDelegate.swift
//  watch Extension
//
//  Created by Norman Laudien on 19.12.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import WatchKit
import WatchConnectivity
import UserNotifications

class ExtensionDelegate: NSObject, WKExtensionDelegate {
    
    override init() {
        super.init()
        setupWatchConnectivity()
    }

    func applicationDidFinishLaunching() {
        // Perform any final initialization of your application.
    }

    func applicationDidBecomeActive() {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillResignActive() {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, etc.
    }

    func handle(_ backgroundTasks: Set<WKRefreshBackgroundTask>) {
        // Sent when the system needs to launch the application in the background to process tasks. Tasks arrive in a set, so loop through and process each one.
        for task in backgroundTasks {
            // Use a switch statement to check the task type
            switch task {
            case let backgroundTask as WKApplicationRefreshBackgroundTask:
                // Be sure to complete the background task once you’re done.
                backgroundTask.setTaskCompletedWithSnapshot(false)
            case let snapshotTask as WKSnapshotRefreshBackgroundTask:
                // Snapshot tasks have a unique completion call, make sure to set your expiration date
                snapshotTask.setTaskCompleted(restoredDefaultState: true, estimatedSnapshotExpiration: Date.distantFuture, userInfo: nil)
            case let connectivityTask as WKWatchConnectivityRefreshBackgroundTask:
                // Be sure to complete the connectivity task once you’re done.
                connectivityTask.setTaskCompletedWithSnapshot(false)
            case let urlSessionTask as WKURLSessionRefreshBackgroundTask:
                // Be sure to complete the URL session task once you’re done.
                urlSessionTask.setTaskCompletedWithSnapshot(false)
            case let relevantShortcutTask as WKRelevantShortcutRefreshBackgroundTask:
                // Be sure to complete the relevant-shortcut task once you're done.
                relevantShortcutTask.setTaskCompletedWithSnapshot(false)
            case let intentDidRunTask as WKIntentDidRunRefreshBackgroundTask:
                // Be sure to complete the intent-did-run task once you're done.
                intentDidRunTask.setTaskCompletedWithSnapshot(false)
            default:
                // make sure to complete unhandled task types
                task.setTaskCompletedWithSnapshot(false)
            }
        }
    }
}

// MARK: - Communication with iPhone app

extension ExtensionDelegate: WCSessionDelegate {
    
    private func setupWatchConnectivity() {
        guard WCSession.isSupported() else { return }
        let session = WCSession.default
        session.delegate = self
        session.activate()
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        NotificationCenter.default.post(
            name: .applicationContextUpdated,
            object: nil
        )
    }
}

// MARK: - Notifications

extension ExtensionDelegate {
    func handleAction(withIdentifier identifier: String?, forRemoteNotification remoteNotification: [AnyHashable : Any]) {
        guard let reportId = remoteNotification["reportId"] as? String else { return }
        guard let jsonString = remoteNotification["report"] as? String else { return }
        guard let jsonData = jsonString.data(using: .utf8) else { return }
        guard let reportData = (try? JSONSerialization.jsonObject(with: jsonData)) as? [String: AnyObject] else { return }
        guard let loc = reportData["l"] as? [Double] else { return }
        let location = CLLocation(latitude: loc[0], longitude: loc[1])
        let report = Report(
            fromKey: reportId,
            andValue: reportData,
            withLocation: location
        )
        let mainIC = WKExtension.shared().rootInterfaceController as! MainInterfaceController
        mainIC.presentController(withName: "ReportInterfaceController", context: report)
    }
}

extension NSNotification.Name {
    static let applicationContextUpdated = NSNotification.Name("applicationContextUpdated")
}
