//
//  ReportRowController.swift
//  watch Extension
//
//  Created by Norman Laudien on 20.12.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import WatchKit

class ReportRowController: NSObject {
    
    var report: Report? {
        didSet {
            guard let report = report else { return }
            titleLabel.setText(report.title)
            subtitleLabel.setText(report.type?.title)
        }
    }
    
    @IBOutlet private weak var titleLabel: WKInterfaceLabel!
    @IBOutlet private weak var subtitleLabel: WKInterfaceLabel!
}
