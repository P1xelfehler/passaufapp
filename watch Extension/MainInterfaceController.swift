//
//  InterfaceController.swift
//  watch Extension
//
//  Created by Norman Laudien on 19.12.18.
//  Copyright © 2018 Omekron. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class MainInterfaceController: WKInterfaceController {
    private var mReports = [Report]()
    
    // MARK: - Outlets
    
    @IBOutlet private weak var reportTable: WKInterfaceTable!
    
    // MARK: - Lifecycle
    
    override func willActivate() {
        super.willActivate()
        fetchReports()
        registerObserver()
    }
    
    override func willDisappear() {
        super.willDisappear()
        unregisterObserver()
    }
    
    // MARK: - Table

    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        let report = mReports[rowIndex]
        presentController(withName: "ReportInterfaceController", context: report)
    }
    
    // MARK: - Data fetching
    
    @objc private func fetchReports() {
        let applicationContext = WCSession.default.receivedApplicationContext
        guard let reportDicts = applicationContext["reports"] as? [String: [String: Any]] else {
            clearData()
            return
        }
        var reports = [Report]()
        for reportDict in reportDicts {
            guard let latitude = reportDict.value[ReportPath.latitude] as? CLLocationDegrees else { continue }
            guard let longitude = reportDict.value[ReportPath.longitude] as? CLLocationDegrees else { continue }
            let location = CLLocation(
                latitude: latitude,
                longitude: longitude
            )
            let report = Report(
                fromKey: reportDict.key,
                andValue: reportDict.value as [String : AnyObject],
                withLocation: location
            )
            reports.append(report)
        }
        loadReports(reports)
        /*DatabaseController.fetchReports { (reports, error) in
            guard let reports = reports else { return } // TODO: show error (or not)
            loadReports(reports)
        }*/
    }
    
    private func loadReports(_ reports: [Report]) {
        self.reportTable.setNumberOfRows(reports.count, withRowType: "reportRow")
        for (index, report) in reports.enumerated() {
            guard let controller = self.reportTable.rowController(at: index) as? ReportRowController else { continue }
            controller.report = report
        }
        self.mReports = reports
    }
    
    private func clearData() {
        mReports.removeAll()
        reportTable.setNumberOfRows(0, withRowType: "reportRow")
    }
    
    // MARK: - Observe data changes
    
    private func registerObserver() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(fetchReports),
            name: .applicationContextUpdated,
            object: nil
        )
    }
    
    private func unregisterObserver() {
        NotificationCenter.default.removeObserver(self)
    }
}
